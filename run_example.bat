@ECHO OFF

SET classparams="bin;lib/jglpk.jar;lib/jnash.jar;lib/jna.jar"

START /B java -cp %classparams% cz.matfyz.minirisk.server.Server -g 50 -n 2
START java -cp %classparams% cz.matfyz.minirisk.clients.sykora.ondrej.MemoryMCClient
START java -cp %classparams% cz.matfyz.minirisk.clients.basic.reactive.ReactiveClient 2
