#!/bin/sh

CLASSPATH_PARAM="-cp bin:lib/jglpk.jar:lib/jnash.jar:lib/jna.jar"

rm -rf bin
mkdir bin

find src -name '*.java' | xargs javac -d bin ${CLASSPATH_PARAM}
