#!/bin/sh

CLASSPATH_PARAM="-cp bin:lib/jglpk.jar:lib/jnash.jar:lib/jna.jar"

java ${CLASSPATH_PARAM} cz.matfyz.isa.jiri.minirisk.server.Server -g 50 -n 5 &

sleep 2

java ${CLASSPATH_PARAM} cz.matfyz.minirisk.clients.sykora.ondrej.MemoryClient > /dev/null &
java ${CLASSPATH_PARAM} cz.matfyz.minirisk.clients.basic.reactive.ReactiveClient 1 > /dev/null &
java ${CLASSPATH_PARAM} cz.matfyz.minirisk.clients.basic.reactive.ReactiveClient 2 > /dev/null &
java ${CLASSPATH_PARAM} cz.matfyz.minirisk.clients.basic.reactive.ReactiveClient 3 > /dev/null &
java ${CLASSPATH_PARAM} cz.matfyz.minirisk.clients.basic.reactive.ReactiveClient 4 > /dev/null
