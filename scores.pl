#!/usr/bin/perl
# Extracts the scores of the clients from all logs (*.txt) files
# in the current directory. Expects that all .txt files are logs
# and that all games have completed successfully.

use strict;
use warnings;

my %clients;

sub process_file {
    my ($file) = @_;

    #open IN, "| tail -n 3 $file";
    #my @lines = <IN>;
    #chomp @lines;
    #close IN;
    my $lines = `tail -n 3 $file`;
    print STDERR "$lines\n";
    my @lines = split /\n/, $lines;

    $lines[0] =~ /total_gain 0 "([^"]*)" ([-0-9]*)/;
    my ($name1, $score1) = ($1, $2);
    $lines[1] =~ /total_gain 1 "([^"]*)" ([-0-9]*)/;
    my ($name2, $score2) = ($1, $2);

    print STDERR "$name1--$name2 : $score1 - $score2\n";
    print STDERR "$name2--$name1 : $score2 - $score1\n";

    $clients{$name1} = $clients{$name1} + $score1;
    $clients{$name2} = $clients{$name2} + $score2;
}

my @files = glob("*.txt");
for my $fn (@files) {
    process_file $fn;
}

for my $key (sort keys %clients) {
    my $score = $clients{$key};
    print "$key\t$score\n";
}
