Number of players: 2-5

THE GAME SEQUENCE:
In the begining of the game every player obtains the currency cards with values from one to fifteen. Other cards (so called "deck cards"), with values -5, -4, -3, -2, -1, 1, 2, ..., 10 are shuffled in the deck.
- In every step of the game a single card from the deck is placed face up on the table.
- Every player puts one of her remaining cards face-down on the table. Every currency card of a player is played exactly once during the game.
- Once all players have done so, all their cards are turned face-up. 
- The winner determined by the method described bellow wins the number of points equal to the value of the card from the deck.

After several repetitions of the game the player with the highest amount of points is the winner.

THE CARD WINNER DETERMINATION METHOD:
Because all players want to collect as many positive points as possible, the player who puts the highest card as a bid for a positive-value deck card wins the points. BUT (!!!) if there are more players bidding the same, they are all skipped and the next best bid gets the chance. This way the highest single (!) bid wins the points.
The method is a bit different if there is a card with the negative value. Because nobody wants it, the lowest single (!!!) bid "wins" it.
If no single bid exists and thus the winner cannot be determined, the card is left on the table and is auctioned together with the next card.
