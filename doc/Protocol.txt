Server communicates with all clients using the TCP connection (listening by default on the port 7654).
Every message between the game server and the game player is a single text line.

1) Player sends its name (used for an identification).
2) Server sends the number of players in the game.
3) The server sends an ID of the player (its index).
4) The server sends a per-move thinking timeout (in milliseconds).

5) Every game is started by the "start" message.
6) Server sends the comma-separated list of card values on the table.
7) Player sends the bid.
8) Server sends a comma-separated list of all bids.
9) Server sends a comma-separated amount of points obtained by each player in the current game step.
10) The game end is announced by the server using the "end" message.

11) The tournament end is announced by the server using the "quit" message.
