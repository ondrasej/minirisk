#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;

my %clients = (
	    "reactive0" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 0",
	    "reactive1" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 1",
	    "reactive2" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 2",
	    "reactive3" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 3",
	    "reactive4" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 4",
	    "reactive5" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 5",
	    "reactive6" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 6",
	    "reactive7" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 7",
	    "reactive8" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 8",
	    "reactive9" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 9",
	    "reactive10" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 10",
	    "reactive11" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 11",
	    "reactive12" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 12",
	    "reactive13" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 13",
	    "reactive14" => "cz.matfyz.isa.jiri.minirisk.client.reactive.ReactiveClient 14",
	    "random" => "cz.matfyz.isa.jiri.minirisk.client.random.RandomClient",
	    "simplerl" => "cz.matfyz.isa.jiri.minirisk.client.simpleRL.SimpleRLClient",
	    "memory-mc" => "cz.matfyz.sykora.ondra.minirisk.client.MemoryMCClient",
	    "memory" => "cz.matfyz.sykora.ondra.minirisk.client.MemoryClient",
	);

my $opt_missing = 0;
my $opt_pretend = 0;

GetOptions(
    "missing" => \$opt_missing,
    "pretend" => \$opt_pretend,
);

sub make_pairs {
    my ($names1, $names2) = @_;
    my $count1 = scalar @$names1;
    my $count2 = scalar @$names2;
    my %pairs_col;
    for(my $i = 0; $i < $count1; $i++) {
	for(my $j = 0; $j < $count2; $j++) {
	    next if $$names1[$i] eq $$names2[$j];
	    my ($nm1, $nm2) = sort ($$names1[$i], $$names2[$j]);
	    $pairs_col{$nm1."\t".$nm2} = 1;;
	}
    }
    return keys %pairs_col;
}


my @names1 = keys %clients;
my @names2 = keys %clients;
my @pairs = make_pairs \@names1, \@names2;

my $pair_count = scalar @pairs;

print STDERR "Total number of pairs: $pair_count\n";

my $start = shift;
my $count = shift;

LOOP: for (my $i = 0; $i < $count; $i++) {
    last if $start + $i >= $pair_count;
    print STDERR "Running $i: ".($start+$i)."\n"; 
    my ($first, $second) = split /\t/, $pairs[$start + $i];
    print STDERR "$first - $second\n";

    if($opt_missing) {
	my $fname;
	if (-f "$first--$second.txt") {
	    $fname = "$first--$second.txt";
	} elsif (-f "$second--$first.txt") {
	    $fname = "$second--$first.txt";
	} else {
	    print STDERR "No file found\n";
	}
	if (defined $fname and -f $fname) {
	    print STDERR "File found: $fname\n";
	    open IN, $fname;
	    my @data = <IN>;
	    close IN;

	    if ($data[-1] =~ /quit/) {
		print STDERR "Skipping $fname\n";
		next LOOP;
	    }
	}
    }

    print STDERR "Running tournament $first--$second\n";
    eval {
	unless ($opt_pretend) {
            system "./run.sh \"".$clients{$first}."\" \"".$clients{$second}."\" $first--$second.txt";
	}
    };
}
