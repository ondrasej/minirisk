/*
The minirisk game server desined to be used as an multi-agent artificial intelligence testbed.
Copyright (C) 2006  Jiri Isa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package cz.matfyz.minirisk.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Random;

/** The Minirisk game server.
  * @author Jiri Isa
  */
public class Server {
	/** The port this server listens on. */
	public static int port = 7654;

	/** The number of players in the game. */
	public static int playersCount = 2;

	/** The number of games in the tournament. */
	public static int tournamentLength = 2000;

	/** The thinking time for each player in every round. In milliseconds. */
	public static int timeout = 0;
	
	/** The initial seed for the random number generator {@link #rnd}. This is
	 * 0 by default, but can be changed in the command-line arguments.
	 */
	public static int seed = 0;

	/** The value of the lowest card. */
	public final static int MIN_CARD = -5;

	/** The value of the highest card. */
	public final static int MAX_CARD = 10;

	/** The amount of deck cards. Also the highest allowed players bid. */
	public final static int MAX_BID = 15;

	/** The amount of cards in the game. Also the number of steps in the game. */
	public final static int CARDS_COUNT = Math.abs(MIN_CARD) + Math.abs(MAX_CARD);

	/** The message signaling the start of a game. */
	public final static String START_MESSAGE = "start";

	/** The message signaling the end of a game. */
	public final static String END_MESSAGE = "end";

	/** The message signaling the end of the tournament. */
	public final static String QUIT_MESSAGE = "quit";

	/** The prefix of the message informing about a new game entrant. */
	public final static String PLAYER_MESSAGE = "player ";

	/** The prefix of the message informing about a single game step winner. */
	public final static String WINNER_MESSAGE = "winner ";

	/** The prefix of the message informing about cards the players bid for in the game step. */
	public final static String CARD_MESSAGE = "card ";

	/** The prefix of the message informing about the points obtained by a player in a single game. */
	public final static String GAME_GAIN_MESSAGE = "game_gain ";

	/** The prefix of a message informing about the points obtained by a player during the whole tournament. */
	public final static String TOTAL_GAIN_MESSAGE = "total_gain ";

	/** The prefix of the game status (number of point of the players). */
	public final static String GAME_STATUS_MESSAGE = "game_status ";

	/** The prefix of the message informing about the bids of all players in a game step. */
	public final static String BIDS_MESSAGE = "bids ";

	/** The prefix of the message informing about cheating. */
	public final static String CHEATING_MESSAGE = "cheating ";

	/** The server socket. */
	ServerSocket serverSocket;

	/** The players' connection sockets. */
	Socket[] clientSockets;

	/** The players' input streams. */
	BufferedReader[] clientIns;

	/** The players' output streams. */
	PrintWriter[] clientOuts;

	/** The players' identification strings as sent by them. */
	String[] clientIDs;

	/** The players' total amount of points. */
	int[] totalGain;

	/** The game deck. */
	ArrayList<Integer> deck = new ArrayList<Integer>(CARDS_COUNT);

	/** The source of randomness. */
	Random rnd;
	
	/** Creates a new instance of the game server listening on the default port. */
	public Server() throws IOException {
		this(port);
	}

	/** Creates a new instance of the game server listening on the given port.
	  * @param port The given port number.
	  * @throws IOException Cannot bind to the port.
	  */
	public Server(int port) throws IOException {
		rnd = new Random(seed);
		serverSocket = new ServerSocket(port);

		clientSockets = new Socket[playersCount];
		clientIns = new BufferedReader[playersCount];
		clientOuts = new PrintWriter[playersCount];
		clientIDs = new String[playersCount];
		totalGain = new int[playersCount];

		initDeck();
	}

	/** Runs the tournament.
	  */
	public void run() throws IOException {
		acceptClients();

		for(int i = 0; i < tournamentLength; i++) {
			playGame(i);
		}

		for(int i = 0; i < playersCount; i++) {
			inform(TOTAL_GAIN_MESSAGE + i + " \"" + clientIDs[i] + "\" " + totalGain[i]);
		}

		sendAll(QUIT_MESSAGE);
		inform(QUIT_MESSAGE);
	}

	/** Sends a message to the given player.
	  * @param index The index of the message receiving player.
	  * @param message The message to be sent.
	  * @throws IOException Message sending failed.
	  */
	protected void send(int index, String message) throws IOException {
		clientOuts[index].println(message);
	}

	/** Broadcasts a message to all players.
	  * @throws IOException Message sending failed.
	  */
	protected void sendAll(String message) throws IOException {
		for(PrintWriter pw : clientOuts) {
			pw.println(message);
		}
	}

	/** Receives a message from the given player.
	  * @param index The index of the player the massage should be received from.
	  * @throws IOException Message receiving failed
	  */
	protected String receive(int index) throws IOException {
		return clientIns[index].readLine();
	}

	/** Receives a message from the given player.
	  * @param index The index of the player the message is received from.
	  * @param timeout The player has this specified amount of time (in milliseconds) to respond.
	  * @throws IOException The message receiving failed.
	  * @throws SocketException The player socket does not allow to set the timeout.
	  */
	protected String receive(int index, int timeout) throws IOException, SocketException {
		clientSockets[index].setSoTimeout(timeout);

		String result = null;
		
		try {
			result = receive(index);
		} catch (SocketTimeoutException e) {
			System.err.println("Player[" + index + "] \"" + clientIDs[index] + "\" did not respond in time.");
			//TODO: Any other idea?
			System.exit(1);
		}

		clientSockets[index].setSoTimeout(0);

		return result;
	}

	/** Gives the information to all who need it.
	  * In the current implementation the message is printed to the standard output.
	  * @throws IOException Should not currently happen.
	  */
	protected void inform(String message) throws IOException {
		System.out.println(message);
	}

	/** Initializes the card deck.
	  */
	private void initDeck() {
		for(int i = MIN_CARD; i <= MAX_CARD; i++) {
			if(i == 0) continue;

			deck.add(new Integer(i));
		}
	}

	/** Shuffles the deck.
	  */
	protected void shuffleDeck() {
		Collections.shuffle(deck, rnd);
	}

	/** Waits for all players to connect and initializes the connections.
	  * @throws IOException Input or output problem.
	  */
	private void acceptClients() throws IOException {
		for(int i = 0; i < playersCount; i++) {
			clientSockets[i] = serverSocket.accept();

			clientIns[i] = new BufferedReader(new InputStreamReader(clientSockets[i].getInputStream()));
			clientOuts[i] = new PrintWriter(clientSockets[i].getOutputStream(), true);

			clientIDs[i] = clientIns[i].readLine();

			inform(PLAYER_MESSAGE + clientIDs[i]);

			send(i, Integer.toString(playersCount));
			send(i, Integer.toString(i));
			send(i, Integer.toString(timeout));
		}
	}

	/** Determines who won the card based on the bids.
	  * @param card The sum of values of the deck cards on the table.
	  * @param bids The bids by all players.
	  * @return The index of the winner, -1 if it cannot be decided.
	  */
	public static int determineWinner(int card, int[] bids) {
		int[] density = new int[CARDS_COUNT + 1];

		for(int i : bids) {
			density[i]++;
		}

		int start = card > 0 ? (density.length - 1) : 0;
		int step = card > 0 ? -1 : +1;

		int winningOffer = -1;
		for(int i = start; i >= 0 && i < density.length; i += step) {
			if(density[i] == 1) {
				winningOffer = i;
				break;
			}
		}

		for(int i = 0; i < bids.length; i++) {
			if(bids[i] == winningOffer) {
				return i;
			}
		}

		return -1;
	}

	/** Plays a single game. */
	private void playGame(int round) throws IOException {
		int[] gameGain = new int[playersCount];
		BitSet[] actionsHistory = new BitSet[playersCount];

		for(int i = 0; i < actionsHistory.length; i++) {
			actionsHistory[i] = new BitSet();
		}

		shuffleDeck();

		//cards left on the table from the previous rounds
		ArrayList<Integer> leftOvers = new ArrayList<Integer>();

		sendAll(START_MESSAGE);
		inform(START_MESSAGE + " " + round);

		for(Integer card : deck) {
			int cardsSum = 0;


			StringBuffer message = new StringBuffer();

			for(Integer leftOver : leftOvers) {
				message.append(leftOver).append(',');
				cardsSum += leftOver.intValue();
			}

			message.append(card);
			cardsSum += card.intValue();

			inform(CARD_MESSAGE + message);
			int[] bids = collectBids(message.toString());
			sendBids(bids);

			for(int i = 0; i < bids.length; i++) {
				if(	bids[i] < 1
					|| bids[i] > MAX_BID
					|| actionsHistory[i].get(bids[i])) {
					inform(CHEATING_MESSAGE + i + " " + clientIDs[i]);
					sendAll(QUIT_MESSAGE);
					//TODO: Any better idea?
					System.exit(1);
				} else {
					actionsHistory[i].set(bids[i]);
				}
			}

			int winner = determineWinner(cardsSum, bids);
			inform(WINNER_MESSAGE + winner);

			sendGains(winner, cardsSum);

			if(winner == -1) {
				leftOvers.add(card);
			} else {
				totalGain[winner] += cardsSum;
				gameGain[winner] += cardsSum;
				leftOvers.clear();
			}
		}

		sendAll(END_MESSAGE);
		inform(END_MESSAGE);

		for(int i = 0; i < playersCount; i++) {
			inform(GAME_GAIN_MESSAGE + i + " " + gameGain[i]);
		}

		for(int i = 0; i < playersCount; i++) {
			inform(GAME_STATUS_MESSAGE + i + " " + totalGain[i]);
		}
	}

	/** Collects the bids from all players.
	  * @param message The message containing a comma-separated list of cards on the table.
	  *                The last value is the newest card on the table.
	  * @return The bids obtained from the players.
	  * @throws IOException Input or output error.
	  */
	private int[] collectBids(String message) throws IOException {
		int[] replies = new int[playersCount];

		for(int i = 0; i < playersCount; i++) {
			send(i, message.toString());
			replies[i] = Integer.parseInt(receive(i, timeout));
		}

		return replies;
	}

	/** Lets all players know about all bids for the last card.
	  * @param bidValues Values of the bids.
	  * @throws IOException Input or output problem.
	  */
	private void sendBids(int[] bidValues) throws IOException {
		String bidMessageStr = intsAsString(bidValues);
		sendAll(bidMessageStr);
		inform(BIDS_MESSAGE + bidMessageStr);
	}

	/** Lets all players know about how much they won in the last game step.
	  */
	private void sendGains(int winner, int value) throws IOException {
		int[] gains = new int[playersCount];
		if(winner >= 0) { //only of someone won this round
			gains[winner] = value;
		}
		sendAll(intsAsString(gains));
	}

	/** Converts a list of integers to a string with their comma-separated values.
	 * @param values The int values.
	 * @return Their string representation.
	 */
	private String intsAsString(int[] values) {
		StringBuffer str = new StringBuffer();
		for(int i = 0; i < values.length; i++) {
			str.append(values[i]);

			if(i < values.length - 1) {
				str.append(',');
			}
		}
		return str.toString();
	}

	/** Runs the server.
	  * Accepted parameters: -p|--port server_port
	  *                      -n|--count number_of_players
	  *                      -g|--games number_of_games_int_the_tournament
	  *                      -t|--timeout thinking_timeout_in _milliseconds
	  *                      -h|--help
	  */
	public static void main(String[] args) throws IOException {
		for(int i = 0; i < args.length; i++) {
			if(args[i].equals("-p") || args[i].equals("--port")) {
				port = Integer.parseInt(args[i+1]);
				i++;
				continue;
			}
			
			if(args[i].equals("-n") || args[i].equals("--count")) {
				playersCount = Integer.parseInt(args[i+1]);
				i++;
				continue;
			}

			if(args[i].equals("-g") || args[i].equals("--games")) {
				tournamentLength = Integer.parseInt(args[i+1]);
				i++;
				continue;
			}

			if(args[i].equals("-t") || args[i].equals("--timeout")) {
				timeout = Integer.parseInt(args[i+1]);
				i++;
				continue;
			}
			
			if(args[i].equals("-s") || args[i].equals("--seed")) {
				seed = Integer.parseInt(args[i+1]);
				i++;
				continue;
			}

			if(args[i].equals("-h") || args[i].equals("--help")) {
				System.err.println("Parameters: [-p|--port port_number] [-n|--count number_of_players] [-g|--games number_of_games] [-t|--timeout thinking_timeout_in_milliseconds] [-h|--help]");
				System.exit(0);
			}
		}

		Server server = new Server();
		server.run();
	}
}
