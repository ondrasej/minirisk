/*
The minirisk game server desined to be used as an multi-agent artificial intelligence testbed.
Copyright (C) 2009  Jiri Isa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package cz.matfyz.minirisk.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/** The replaying Minirisk game server.
  * @author Jiri Isa
  */
public class ReplayServer extends Server {
	/* The deck cards as they appeared in the log. */
	protected Queue<Integer> sequence = null;

	/** Creates a new instance of the game server listening on the default port. */
	public ReplayServer(Queue<Integer> cards) throws IOException {
		this(cards, port);
	}

	/** Creates a new instance of the game server listening on the given port.
	  * @param port The given port number.
	  * @throws IOException Cannot bind to the port.
	  */
	public ReplayServer(Queue<Integer> cards, int port) throws IOException {
		super(port);
		this.sequence = cards;
		tournamentLength = cards.size() / CARDS_COUNT;
	}

	/** Shuffles the deck.
	  */
	protected void shuffleDeck() {
		deck.clear();
		for(int i = 0; i < CARDS_COUNT; i++) {
			deck.add(this.sequence.remove());
		}
	}

	protected static Queue<Integer> readCards(String logfilename) throws IOException {
		Queue<Integer> cards = new LinkedList<Integer>();
		BufferedReader br = new BufferedReader(new FileReader(logfilename));

		for(String line = br.readLine(); line != null; line = br.readLine()) {
			if(line.startsWith(CARD_MESSAGE)) {
				String[] crds = line.split(" ")[1].split(",");
				Integer card = Integer.valueOf(crds[crds.length-1]); //the last card is the new one
				cards.add(card);
			}
		}

		return cards;
	}

	/** Runs the server.
	  * Accepted parameters: -l|--log logfile [required]
	  *                      -p|--port server_port
	  *                      -n|--count number_of_players
	  *                      -t|--timeout thinking_timeout_in _milliseconds
	  *                      -h|--help
	  */
	public static void main(String[] args) throws IOException {
		String logfilename = null;
		String helpString = "Parameters: -l|--log logfile [-p|--port port_number] [-n|--count number_of_players] [-t|--timeout thinking_timeout_in_milliseconds] [-h|--help]";

		for(int i = 0; i < args.length; i++) {
			if(args[i].equals("-l") || args[i].equals("--log")) {
				logfilename = args[i+1];
				i++;
				continue;
			}


			if(args[i].equals("-p") || args[i].equals("--port")) {
				port = Integer.parseInt(args[i+1]);
				i++;
				continue;
			}
			
			if(args[i].equals("-n") || args[i].equals("--count")) {
				playersCount = Integer.parseInt(args[i+1]);
				i++;
				continue;
			}

			if(args[i].equals("-t") || args[i].equals("--timeout")) {
				timeout = Integer.parseInt(args[i+1]);
				i++;
				continue;
			}

			if(args[i].equals("-h") || args[i].equals("--help")) {
				System.err.println(helpString);
				System.exit(0);
			}
		}

		if(logfilename == null) {
			System.err.println("Missing the \"-l|--log logfile\" parameter.");
			System.exit(1);
		}

		Server server = new ReplayServer(readCards(logfilename));
		server.run();
	}
}
