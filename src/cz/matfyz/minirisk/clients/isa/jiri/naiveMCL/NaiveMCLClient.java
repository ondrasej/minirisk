/*
   A naive Monte-Carlo minirisk player.
   Copyright (C) 2007  Jiri Isa

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package cz.matfyz.minirisk.clients.isa.jiri.naiveMCL;

import cz.matfyz.minirisk.client.Client;
import cz.matfyz.minirisk.server.Server;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Random;

/** This is a naive Monte-Carlo minirisk player.
  * In every game step it tries many possible game futures randomly.
  * Both the deck cards future and the players' actions. All the actions
  * are generated with uniform distribution (includin the own actions),
  * hence "naive".
  */
public class NaiveMCLClient extends Client {
	/** Number of trials for the MCL approximation. */
	protected final static int MCL_TRIALS = 40000;

	/** Deck cards that already appeared in the current game. */
	protected BitSet deckCardsGone = new BitSet(CARDS_COUNT + 1);

	/** Bid cards that were already played in the current game. */
	protected BitSet[] bidCardsGone;

	/** Random numbers generator. */
	protected static Random rnd = new Random();

	/** Accumulated current game profit.*/
	private int profit = 0;

	/** The current game round. */
	private int round = 0;

	/** Creates a new instance of the player.
	 * Cnnects to the server on localhost and on the default port.
	 * @throws UnknownHostException When the hostname is not known.
	 * @throws IOException Input/output related problem.
	 */
	public NaiveMCLClient() throws UnknownHostException, IOException {
		super("NaiveMCL");
		init();
	}

	/** Creates a new instance of the player.
	 * Connects to the game server on the computer with the given hostname and listening on the given port.
	 * @param hostname Game server hostname.
	 * @param port Game server port.
	 * @throws UnknownHostException When the game server hostname is not nown.
	 * @throws IOException Input/output error.
	 */
	public NaiveMCLClient(String hostname, int port) throws UnknownHostException, IOException {
		super(hostname, port, "NaiveMCL");
		init();
	}

	/** Initializes this player.
	 */
	protected void init() {
		int count = getPlayersCount();
		bidCardsGone = new BitSet[count];
		for(int i = 0; i < count; i++) {
			bidCardsGone[i] = new BitSet(CARDS_COUNT + 1);
		}
	}

	/** When the new game starts, this function is invoked.
	 */
	protected void start() {
		deckCardsGone.clear();
		for(BitSet b : bidCardsGone) {
			b.clear();
		}
		profit = 0;
		round = 0;
	}

	/** The function invoked to react to the cards on the table.
	  * This is the MCL hearth of this player.
	  * The iteration over many possible futures is performed randomly for every currently possible bid action.
	  * The bid action with the highest expeted utility is played.
	  * All the future actions are selected with the uniform distribution. Including the own actions.
	  */
	@SuppressWarnings("unchecked")
	public int play(int[] cards) {
		ArrayList<Integer>[] strategies = new ArrayList[getPlayersCount()];
		ArrayList<Integer> deckFuture = new ArrayList<Integer>(CARDS_COUNT - deckCardsGone.cardinality());

		// fill the strategies with available bid cards
		for(int i = 0; i < bidCardsGone.length; i++) {
			strategies[i] = new ArrayList<Integer>(CARDS_COUNT - bidCardsGone[i].cardinality());

			for(int bid = bidCardsGone[i].nextClearBit(1);
					bid <= CARDS_COUNT;
					bid = bidCardsGone[i].nextClearBit(bid + 1)) {
				strategies[i].add(Integer.valueOf(bid));
			}
		}

		// fill the deck card future with deck cards not played so far and cards on the table now
		int deckCardsSum = 0;
		for(int deckCard : cards) {
			deckCardsGone.set(normalizeCard(deckCard));
			deckCardsSum += deckCard;
		}
		deckFuture.add(Integer.valueOf(deckCardsSum));
		for(int deckFutureCard = deckCardsGone.nextClearBit(0);
				deckFutureCard < CARDS_COUNT;
				deckFutureCard = deckCardsGone.nextClearBit(deckFutureCard + 1)) {
			deckFuture.add(Integer.valueOf(denormalizeCard(deckFutureCard)));
		}

		// MCL - try many possible futures and strategies
		int id = getID();
		int bestBid = 0;
		double bestUtility = Double.NEGATIVE_INFINITY;
		int[] bids = new int[strategies.length]; //bid of all player. Alocated outside the loops to spare time and memory.
		double expectedUtility = 0; //expected utility of a bid. Allocated outside the bids loop to spare time and memory.
		int trials = MCL_TRIALS / 2 + (MCL_TRIALS / 2) * (CARDS_COUNT - round) / CARDS_COUNT;
		for(int ownBid = bidCardsGone[id].nextClearBit(1);
				ownBid <= CARDS_COUNT;
				ownBid = bidCardsGone[id].nextClearBit(ownBid + 1)) {
			expectedUtility = 0;

			for(int mcl = 0; mcl < trials; mcl++) {
				for(ArrayList<Integer> strategy : strategies) {
					Collections.shuffle(strategy, rnd);
				}

				//find the ownBid option in the own strategy and move it to the first position
				ArrayList<Integer> ownStrategy = strategies[id];
				for(int i = 0; i < ownStrategy.size(); i++) {
					if(ownStrategy.get(i).intValue() == ownBid) {
						ownStrategy.set(i, ownStrategy.get(0));
						ownStrategy.set(0, Integer.valueOf(ownBid));
						break;
					}
				}

				// Shuffle the future, swap the current situation to the beginning
				Collections.shuffle(deckFuture, rnd);
				for(int i = 0; i < deckFuture.size(); i++) {
					if(deckFuture.get(i).intValue() == deckCardsSum) {
						deckFuture.set(i, deckFuture.get(0));
						deckFuture.set(0, Integer.valueOf(deckCardsSum));
						break;
					}
				}

				// Evaluate the strategies-future combination
				assert(deckFuture.size() == ownStrategy.size());
				int cardsSum = 0;
				for(int round = 0; round < deckFuture.size(); round++) {
					for(int i = 0; i < strategies.length; i++) {
						bids[i] = strategies[i].get(round).intValue();
					}

					cardsSum += deckFuture.get(round).intValue();
					int winner = Server.determineWinner(cardsSum, bids);
					if(winner == id) {
						expectedUtility += cardsSum;
					}

					if(winner != -1) {
						cardsSum = 0;
					}
				}
			}

			//because every own bid action receives the same amount of trials, it is not needed to divide to utility to get the "expected" value
			if(expectedUtility > bestUtility) {
				bestUtility = expectedUtility;
				bestBid = ownBid;
			}
		}

		round++;

		return bestBid;
	}

	/** This function receives all players' reactions to the last card on the table.
	 * @param values The reactions of all players. This player's reaction has the index
	 *               corresponding to this player's ID.
	 */
	protected void bids(int[] values) {
		assert(values.length == bidCardsGone.length);

		for(int i = 0; i < values.length; i++) {
			bidCardsGone[i].set(values[i]);
		}
	}

	/** This function is invoked to inform the player about the gains obtained in the last round.
	  * @param values The obtained points.
	  */
	protected void gains(int[] values) {
		int value = values[getID()];
		System.out.println("I just received " + value + " points.");
		profit += value;
	}

	/** When the game ends, this function is invoked.
	  */
	protected void end() {
		System.out.println("A game just ended and I won " + profit + " points.");
	}

	/** Converts the deck card value from the MIN_CARD, ..., -1, 1, ..., MAX_CARD values to the 0, ..., CARDS_COUNT - 1 values.
	  * @param deckCard The deck card value to be converted.
	  * @return The normalized deck card value.
	  * @see denormalizeCard
	  */
	protected static int normalizeCard(int deckCard) {
		assert(deckCard >= MIN_CARD);
		assert(deckCard <= MAX_CARD);
		assert(deckCard != 0);

		int result = deckCard - MIN_CARD;

		if(deckCard > 0) {
			result--;
		}

		return result;
	}

	/** Converts the  deck card value from the 0, ..., CARDS_COUNT - 1 values to the MIN_CARD, ..., -1, 1, ..., MAX_CARD values.
	  * @param deckCard deckCard to be converted.
	  * @return The denormalized deck card value.
	  * @see normalizeCard
	  */
	protected static int denormalizeCard(int deckCard) {
		assert(deckCard >= 0);
		assert(deckCard < CARDS_COUNT);

		int result = deckCard + MIN_CARD;

		if(result >= 0) {
			result++;
		}

		return result;
	}

	/** Runs the player.
	 * @param args The command line parameters.
	 * <ul>
	 *   <li>The first one could be the game server hostname. By default "localhost".</li>
	 *   <li>The second one could be the game server port. By default 7654.</li>
	 * </ul>
	 * If any parameter is to be used, all preceding ones have to be specified.
	 */
	public static void main(String[] args) throws Exception {
		String hostname = (args.length > 0 ? args[0] : DEFAULT_HOSTNAME);
		int port = (args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_PORT);

		Client client = new NaiveMCLClient(hostname, port);
		client.run();
	}
}
