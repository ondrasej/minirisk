/*
A simple reactive minirisk player.
Copyright (C) 2010  Jiri Isa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package cz.matfyz.minirisk.clients.isa.jiri.sliding;

import cz.matfyz.minirisk.client.Client;
import cz.matfyz.minirisk.clients.basic.reactive.ReactiveClient;

public class SlidingClient extends ReactiveClient {
	/** Creates a new instance of the player.
	  * @throws Exception In case of the connection problem.
	  */
	public SlidingClient() throws Exception {
		super("SlidingClient");
	}

	/** Creates a new instance of the player.
	  * @param hostname Game server host name.
	  * @param port Game server port.
	  * @throws Exception In case of the connection problem.
	  */
	public SlidingClient(String hostname, int port) throws Exception {
		super(hostname, port, "SlidingClient");
	}

	/** This function is invoked when the game round ends. */
	protected void end() {
		this.strategyShift = (this.strategyShift + 1) % CARDS_COUNT;
	}

	/** Runs the player.
	  * @param args The command line parameters.
	  * <ul>
	  *   <li>The first one could be the game server hostname. By default "localhost".</li>
	  *   <li>The second one could be the game server port. By default 7654.</li>
	  * </ul>
	  * If any parameter is to be used, all preceding ones have to be specified.
	  */
	public static void main(String[] args) throws Exception {
		String hostname = (args.length > 0 ? args[0] : DEFAULT_HOSTNAME);
		int port = (args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_PORT);

		Client client = new SlidingClient(hostname, port);
		client.run();
	}
}
