package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.lang.StringBuilder;
import java.lang.System;
import java.util.ArrayList;
import java.util.Arrays;

public class Factor {
	protected int[] factors;

	public Factor(Factor f) {
		this.factors = new int[f.factors.length];
		System.arraycopy(f.factors, 0, this.factors, 0, f.factors.length);
	}

	public Factor(GameState state) {
		assert(1 == state.getNumberOfOpponents());
		int[] myActions = state.myAvailableBids();
		int[] herActions = state.othersAvailableBids()[0];

		assert(myActions.length == herActions.length);

		this.factors = new int[2*myActions.length + 1];
		int j = 0;
		int control = 0;
		for(int i = 0; i < myActions.length; i++) {
			/* Count the opponents cards smaller the my i-th card is. */
			for(; j < herActions.length && herActions[j] < myActions[i]; j++) {
				factors[2*i]++;
			}

			/* Find out if the opponent has the same card as my i-th is. */
			for(; j < herActions.length && herActions[j] == myActions[i]; j++) {
				factors[2*i + 1] = 1; //happens at most once
			}

			control += factors[2*i] + factors[2*i+1];
		}
		/*add all the cards bigger then all mine*/
		for(; j < herActions.length; j++) {
			factors[2*myActions.length]++;
			control++;
		}

		if(control != herActions.length) {
			System.err.println(Arrays.toString(myActions));
			System.err.println(Arrays.toString(herActions));
			System.err.println(Arrays.toString(factors));
		}
		assert(control == herActions.length);

		/*
		System.err.println("ma: " + Arrays.toString(myActions));
		System.err.println("ha: " + Arrays.toString(herActions));
		System.err.println("fa: " + Arrays.toString(factors));
		System.err.println("mf: " + Arrays.toString(myAvailableBids()));
		System.err.println("hf: " + Arrays.toString(herAvailableBids()));
		*/
	}

	public Factor(String s) {
		String[] items = s.split(";");
		this.factors = new int[items.length];
		for(int i = 0; i < this.factors.length; i++) {
			this.factors[i] = Integer.parseInt(items[i]);
		}
	}

	public Object clone() {
		return new Factor(this);
	}

	public Factor next(int myAction, int herAction) {
		Factor n = (Factor) this.clone();

		/* remove the opponents card */
		n.factors[herAction]--;
		/* merge the factors which were previously split by myAction */
		n.factors[myAction-1] += n.factors[myAction] + n.factors[myAction+1];
		int[] new_factors = new int[n.factors.length - 2];
		/* take the factors before the split */
		System.arraycopy(n.factors, 0, new_factors, 0, myAction);
		/* take the factors after the split */
		System.arraycopy(n.factors, myAction + 2, new_factors, myAction, new_factors.length - myAction); //I hope this works (TODO).
		/* use the new factors */
		n.factors = new_factors;

		/*
		System.err.println("f: " + Arrays.toString(this.factors));
		System.err.println("a: " + myAction + " " + herAction);
		System.err.println("f': " + Arrays.toString(n.factors));
		*/

		return n;
	}

	public int[] getFactors() {
		return this.factors;
	}

	public int[] myAvailableBids() {
		int max = this.factors.length / 2; //half of the factors (every second) are mine
		ArrayList<Integer> l = new ArrayList<Integer>(max);

		if(max > 0) {
			l.add(1); //the lowest action cannot be merged with any other
		}

		for(int i = 1; i < max; i++) {
			int cnt = 0;
			for(int j = 0; j < 3; j++) {
				cnt += this.factors[2*i + 1 - j];
			}
			if(cnt == 0) { //this action can be merged with a lower one
				; //the lower action is already in the list
			} else { //a new unique action
				l.add(2*i+1);
			}
		}

		int[] result = new int[l.size()];
		for(int i = 0; i < result.length; i++) {
			result[i] = l.get(i);
		}

		return result;
	}

	public int[] myAvailableBidsRepeated() {
		int max = this.factors.length / 2; //half of the factors (every second) are mine
		int[] bids = new int[max];

		if(max == 0) {
			return bids;
		}

		bids[0] = 1; //the lowest action cannot be merged with any other
		for(int i = 1; i < max; i++) {
			int cnt = 0;
			for(int j = 0; j < 3; j++) {
				cnt += this.factors[2*i + 1 - j];
			}
			if(cnt == 0) { //this action can be merged with a lower one
				; //the lower action is already in the list
				bids[i] = bids[i-1]; //repeat the lower action
			} else { //a new unique action
				bids[i] = 2 * i + 1;
			}
		}

		return bids;
	}

	public int[] herAvailableBids() {
		int cnt = 0;
		for(int i = 0; i < this.factors.length; i++) {
			if(this.factors[i] > 0) {
				cnt++;
			}
		}
		int[] bids = new int[cnt];
		int id = 0;
		for(int i = 0; i < this.factors.length; i++) {
			if(factors[i] > 0) {
				bids[id] = i;
				id++;
			}
		}
		assert(id == cnt);
		return bids;
	}

	public int[] herAvailableBidsRepeated() {
		int cnt = 0;
		for(int i = 0; i < this.factors.length; i++) {
			cnt += this.factors[i];
		}
		int[] bids = new int[cnt];
		int id = 0;
		for(int i = 0; i < this.factors.length; i++) {
			for(int j = 0; j < this.factors[i]; j++) {
				bids[id] = i;
				id++;
			}
		}
		assert(id == cnt);
		return bids;
	}

	public int depth() {
		return this.factors.length / 2; //number of my moves left
	}

	public int hashCode() {
		int result = 0;
		for(int f : this.factors) {
			result *= 15;
			result += f;
		}
		return result;
	}

	public boolean equals(Object other) {
		Factor ot = (Factor) other;
		return Arrays.equals(this.factors, ot.factors);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < this.factors.length; i++) {
			sb.append(this.factors[i]);

			if(i < this.factors.length - 1) {
				sb.append(';');
			}
		}
		return sb.toString();
	}
}
