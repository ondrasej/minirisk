package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.util.Arrays;

public class Strategy {
	public int[] actions;

	public Strategy(int[] actions) {
		this.actions = (int[]) actions.clone();
	}

	public int hashCode() {
		return Arrays.hashCode(this.actions);
	}

	public boolean equals(Object other) {
		Strategy ot = (Strategy) other;
		return Arrays.equals(this.actions, ot.actions);
	}
}
