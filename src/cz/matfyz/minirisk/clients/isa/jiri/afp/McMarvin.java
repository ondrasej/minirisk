package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.util.Arrays;
import java.util.Random;

public class McMarvin extends Marvin {
	public McMarvin() {
		super();
	}

	public McMarvin(Random rnd) {
		super(rnd);
	}

	public static String getName() {
		return "McMarvin";
	}

	protected double eval(int[] myStrategy, int[] herStrategy, int[] cards, int leftOver) {
		final int MC_COUNT = 3;
		double result = 0.0;
		int[] sequence = (int[]) cards.clone();

		int cnt = MC_COUNT; //TODO: less cycles with fewer cards
		for(int k = 0; k < cnt; k++) {
			mix(sequence);
			result += evalSequence(sequence, myStrategy, herStrategy, cards, leftOver) / (double) cnt;
		}

		return result;
	}

	protected double evalSequence(int[] sequence, int[] myStrategy, int[] herStrategy, int[] cards, int leftOver) {
		int[] myActions = actions(sequence, myStrategy, cards);
		int[] herActions = actions(sequence, herStrategy, cards);
		return evalTrace(leftOver, myActions, herActions, sequence); //TODO eval only the beginning, use the endgame database for an end
	}

	protected int[] actions(int[] sequence, int[] strategy, int[] cards) {
		assert(sequence.length == strategy.length);
		assert(sequence.length == cards.length);
		int[] result = new int[sequence.length];

		for(int i = 0; i < result.length; i++) {
			for(int j = 0; j < cards.length; j++) {
				if(cards[j] == sequence[i]) {
					result[i] = strategy[j];
					break;
				}
			}
		}

		return result;
	}
}
