package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.util.Arrays;

class Situation {
	private Factor f;
	private int[] cards;
	private int leftOver;

	public Situation(Factor f, int[] cards, int leftOver) {
		this.f = (Factor) f.clone();
		this.cards = (int[]) cards.clone();
		this.leftOver = leftOver;
	}

	public int hashCode() {
		return this.f.hashCode() * 6276021 + Arrays.hashCode(this.cards) * 341 + leftOver;
	}

	public boolean equals(Object other) {
		Situation ot = (Situation) other;

		return this.f.equals(ot.f) && Arrays.equals(this.cards, ot.cards) && (this.leftOver == ot.leftOver);
	}

	public String toString() {
		return this.f.toString() + " :: " + Arrays.toString(this.cards) + " :: " + this.leftOver;
	}

	public Factor getFactor() {
		return this.f;
	}

	public int[] getCards() {
		return this.cards;
	}

	public int getLeftOver() {
		return this.leftOver;
	}
}
