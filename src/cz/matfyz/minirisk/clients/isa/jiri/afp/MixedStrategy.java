package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.lang.System;
import java.util.Random;

public class MixedStrategy {
	private int[] actions = null;
	private double[] probs = null;
	private double expectedUtility;
	private Random defaultRnd = new Random(42);

	public MixedStrategy(int[] actions, double[] probs, double expectedUtility) {
		this.actions = actions;
		this.probs = probs;
		this.expectedUtility = expectedUtility;
	}

	public double getExpectedUtility() {
		return this.expectedUtility;
	}

	public int sample() {
		return sample(this.defaultRnd);
	}

	public int sample(Random rnd) {
		double r = rnd.nextDouble();
		int i = 0;

		while((i < probs.length) && (probs[i] <= r)) {
			r -= probs[i];
			i++;
		}

		if(i == probs.length) {
			i--;
		}

		//System.err.println("S: " + i);

		return actions[i];
	}

	public void unfactor(int[] origActions) {
		for(int i = 0; i < this.actions.length; i++) {
			this.actions[i] = origActions[this.actions[i] / 2]; //a factored action equals to 2*index+1
		}
	}
}
