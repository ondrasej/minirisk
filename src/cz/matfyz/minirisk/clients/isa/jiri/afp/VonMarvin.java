package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.util.Random;

public class VonMarvin extends Marvin {
	public VonMarvin() {
		super();
	}

	public VonMarvin(Random rnd) {
		super(rnd);
	}

	public static String getName() {
		return "VonMarvin";
	}

	protected double eval(int[] myStrategy, int[] herStrategy, int[] cards, int leftOver) {
		/* the following is an extremely rough approximation of the expected value.
		 * All pass-throughs are considered to be independent events (which is incorrect).
		 * The cards passed through are considered not to influence who wins in the next turn(s)
		 * (which is incorrect).
		 */
		assert(myStrategy.length == herStrategy.length);
		assert(myStrategy.length == cards.length);

		int nlower = 0, nsame = 0, nbigger = 0;
		for(int i = 0; i < myStrategy.length; i++) {
			if(myStrategy[i] == herStrategy[i]) {
				nsame++;
			} else if(myStrategy[i] > herStrategy[i]) {
				nbigger++;
			} else {
				nlower++;
			}
		}

		if(nlower + nbigger == 0) { //a trivial case
			int sum = leftOver;
			for(int card : cards) {
				sum += card;
			}

			return sum / 2.0; //all the cards fall through
		}

		double pfallthrough = 0.0; //probability that the card is either a last one, or all following actions are same
		int n = cards.length;
		for(int i = 0; i < nsame; i++) {
			pfallthrough += (1.0 / n) * (1.0 / product(n - i + 1, n)) * product(nsame - i, nsame - 1);
		}
		double pwin = (1.0 - pfallthrough) * nbigger / (double) (nbigger + nlower);
		double ploose = (1.0 - pfallthrough) * nlower / (double) (nbigger + nlower);

		double result = 0;
		for(int i = 0; i < cards.length; i++) {
			if((cards[i] > 0) && (myStrategy[i] > herStrategy[i])) {
				result += cards[i];
			} else if((cards[i] < 0) && (myStrategy[i] < herStrategy[i])) {
				result += cards[i];
			} else if(myStrategy[i] == herStrategy[i]) {
				if(cards[i] > 0) {
					result += pwin * cards[i];
				} else if(cards[i] < 0) {
					result += ploose * cards[i];
				} else {
					assert(false); //should not get here
				}
			}
		}
		result += pwin * leftOver;

		return result;
	}
}
