package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.lang.Runtime;
import java.lang.System;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import cz.matfyz.isa.jiri.jnash.SolutionSingle;
import cz.matfyz.isa.jiri.jnash.TwoPlayerConstSum;

public class GameTree {
	protected final static int MAX_CACHED_DEPTH = 6;
	protected final static int IMPOSSIBLY_LOW_GAIN = -10000;
	protected ChanceNode root = null;
	protected static Map<Situation,ChanceNode> cache = new HashMap<Situation,ChanceNode>();

	public GameTree(Factor f, int[] cards, int leftOver) {
		//TODO: refactor
		Situation situation = new Situation(f, cards, leftOver);
		this.root = cache.get(situation);
		if(this.root == null) {
			this.root = new ChanceNode(f, cards, leftOver, IMPOSSIBLY_LOW_GAIN);
			maybeStore(situation, this.root);
		}
	}

	protected static void maybeStore(Situation situation, ChanceNode node) {
		final long FREE_MEMORY_SAFETY_MARGIN = 1024l*1024l*100; //100MB
		if(situation.getFactor().depth() <= MAX_CACHED_DEPTH
				/*&& (node.getUtility() > IMPOSSIBLY_LOW_GAIN) */
				) {
			if(Runtime.getRuntime().freeMemory() + Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory() < FREE_MEMORY_SAFETY_MARGIN) {
				//System.err.println("Before: " + (Runtime.getRuntime().freeMemory() + Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()));
				cache.clear();
				Runtime.getRuntime().gc();
				//System.err.println("After: " + (Runtime.getRuntime().freeMemory() + Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()));
			}

			cache.put(situation, node);
		}
	}

	public double getUtility() {
		return this.root.getUtility();
	}

	protected class Node {
		protected double utility = 0.0;
		public double getUtility() { return this.utility; }

		protected int maxRemainingGain(Factor f, int[] cards) {
			int[] myActions = f.myAvailableBidsRepeated();
			int[] herActions = f.herAvailableBidsRepeated();
			Marvin.revert(myActions);
			int result = 0;
			assert(myActions.length == herActions.length);
			//cards.length <= myActions.length == herActions.length (because some cards may already be on the deck)
			for(int i = 0; (i < cards.length) && (myActions[i] >= herActions[i]) && (cards[i] > 0); i++) {
				result += cards[i];
			}
			return result;
		}
	}

	protected class ChanceNode extends Node {
		public ChanceNode(Factor f, int[] cards, int leftOver, double bestGainSoFar) {
			/*
			if(leftOver + maxRemainingGain(f, cards) < bestGainSoFar) { //no chance to be better
				this.utility = IMPOSSIBLY_LOW_GAIN;
				return;
			}
			*/
			DecisionNode[] subnodes = null;
			if(cards.length == 0) {
				this.utility = leftOver / 2.0;
			} else {
				subnodes = new DecisionNode[cards.length];

				for(int i = 0; i < cards.length; i++) {
					int[] new_cards = nextCards(cards, i);
					/*
					System.err.println("c: " + Arrays.toString(cards));
					System.err.println(i);
					System.err.println("nc: " + Arrays.toString(new_cards));
					*/
					int deck = leftOver + cards[i];
					subnodes[i] = new DecisionNode(f, new_cards, deck, bestGainSoFar);
					//System.err.println(i + ": " + subnodes[i].getUtility());
					this.utility += subnodes[i].getUtility() / subnodes.length;
				}
			}

			//System.err.println("out: " + this.utility + " | " + Arrays.toString(cards) + " | " + leftOver);
		}

		protected int[] nextCards(int[] cards, int index) {
			int[] result = new int[cards.length-1];
			for(int i = 0; i < cards.length; i++) {
				if(i == index) continue;
				result[i < index ? i : i-1] = cards[i];
			}
			return result;
		}
	}

	protected class DecisionNode extends Node {

		public DecisionNode(Factor f, int[] cards, int deck, double bestGainSoFar) {
			/*
			if(deck + maxRemainingGain(f, cards) < bestGainSoFar) {
				this.utility = IMPOSSIBLY_LOW_GAIN;
				return;
			}
			*/
			int[] myActions = f.myAvailableBids();
			int[] herActions = f.herAvailableBids();
			ChanceNode[][] subnodes = new ChanceNode[myActions.length][herActions.length];
			double[][] payoffs = new double[myActions.length][herActions.length];

			for(int i = 0; i < myActions.length; i++) {
				for(int j = 0; j < herActions.length; j++) {
					Factor new_f = f.next(myActions[i], herActions[j]);
					int leftOver = (myActions[i] == herActions[j] ? deck : 0);
					int rew = reward(myActions[i], herActions[j], deck);
					Situation situation = new Situation(new_f, cards, leftOver);
					subnodes[i][j] = cache.get(situation);
					if(subnodes[i][j] == null) {
						subnodes[i][j] = new ChanceNode(new_f, cards, leftOver, bestGainSoFar - rew);
						maybeStore(situation, subnodes[i][j]);
					}
					payoffs[i][j] = subnodes[i][j].getUtility() + rew;
					//System.err.println(i + "," + j + " " + subnodes[i][j].getUtility() + " " + rew);
					bestGainSoFar = Math.max(bestGainSoFar, payoffs[i][j]);
				}
			}
			
			SolutionSingle solution = TwoPlayerConstSum.solveSingle(payoffs);
			this.utility = solution.getUtility();

			//System.err.println("dut: " + this.utility + " | " + Arrays.toString(cards) + " | " + deck + " # " + Arrays.toString(myActions) + " " + Arrays.toString(herActions));
		}

		protected int reward(int myAction, int herAction, int deck) {
			if(deck > 0) {
				if(myAction > herAction) {
					return deck;
				}
			} else {
				if(myAction < herAction) {
					return deck;
				}
			}
			return 0;
		}
	}
}
