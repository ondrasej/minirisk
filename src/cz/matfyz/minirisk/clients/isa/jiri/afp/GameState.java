package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.lang.System;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import cz.matfyz.minirisk.client.Client;

public class GameState {
	private final static int CARDS_COUNT = 15;
	private final static int MIN_CARD = -5;

	private int id = -1;
	private boolean[] cards = new boolean[CARDS_COUNT];
	private boolean[][] bidsLeft = null;
	private int[] score = null;
	private int[] deck = null;

	public GameState(int playersCount, int id) {
		this.id = id;
		Arrays.fill(this.cards, true);
		this.bidsLeft = new boolean[playersCount][CARDS_COUNT];
		for(boolean[] bl : this.bidsLeft) {
			Arrays.fill(bl, true);
		}
		this.score = new int[playersCount];
	}

	public void update(int[] bids, int[] gains) {
		assert(this.bidsLeft.length == bids.length);
		for(int i = 0; i < bids.length; i++) {
			this.bidsLeft[i][bids[i]-1] = false;
		}

		assert(this.score.length == gains.length);
		for(int i = 0; i < gains.length; i++) {
			this.score[i] += gains[i];
		}
	}

	public void setDeck(int[] cards) {
		for(int card : cards) {
			this.cards[toId(card)] = false;
		}

		this.deck = (int[]) cards.clone();
	}

	public int[] getDeck() {
		return this.deck;
	}

	protected int toId(int card) {
		if(card > 0) card--;
		return card - MIN_CARD;
	}

	protected int fromId(int id) {
		int card = id + MIN_CARD;
		if(card >= 0) card++;
		return card;
	}

	public int[] myAvailableBids() {
		int[] bids = valuesLeft(this.bidsLeft[this.id]);
		for(int i = 0; i < bids.length; i++) {
			bids[i]++; //bids are 1-based, not 0-based
		}
		return bids;
	}

	public int[][] othersAvailableBids() {
		List<int[]> others = new ArrayList<int[]>(this.bidsLeft.length - 1);

		for(int i = 0; i < this.bidsLeft.length; i++) {
			if(i == this.id) continue;

			int[] bids = valuesLeft(this.bidsLeft[i]);
			for(int j = 0; j < bids.length; j++) {
				bids[j]++; //1-based bids
			}
			others.add(bids);
		}

		int[][] result = new int[others.size()][];
		for(int i = 0; i < result.length; i++) {
			result[i] = others.get(i);
		}

		return result;
	}

	public int myScore() {
		return this.score[this.id];
	}

	public int[] othersScores() {
		int[] result = new int[this.score.length - 1];

		for(int i = 0; i < this.score.length; i++) {
			if(i == this.id) continue;

			result[i < this.id ? i : i - 1] = this.score[i];
		}

		return result;
	}

	public int myId() {
		return this.id;
	}

	public int getNumberOfOpponents() {
		return this.score.length - 1;
	}

	public int[] getCards() {
		int[] cards = valuesLeft(this.cards);
		for(int i = 0; i < cards.length; i++) {
			cards[i] = fromId(cards[i]);
		}
		return cards;
	}

	protected int[] valuesLeft(boolean[] flags) {
		List<Integer> values = new ArrayList<Integer>(flags.length);
		for(int i = 0; i < flags.length; i++) {
			if(flags[i]) {
				values.add(i);
			}
		}

		int[] result = new int[values.size()];
		for(int i = 0; i < result.length; i++) {
			result[i] = values.get(i);
		}

		return result;
	}

	public GameState restrict(int opponent) {
		if(opponent >= this.id) opponent++; //the outer world has a bit skewed ideas about the ideas (they do not include self)
		GameState result = new GameState(2, 0); //Create a two-player state, id 0 being self, id 1 being the opponent

		assert(this.cards.length == result.cards.length);
		System.arraycopy(this.cards, 0, result.cards, 0, this.cards.length);
		assert(this.bidsLeft[this.id].length == result.bidsLeft[0].length);
		System.arraycopy(this.bidsLeft[this.id], 0, result.bidsLeft[0], 0, this.bidsLeft[this.id].length);
		assert(this.bidsLeft[opponent].length == result.bidsLeft[1].length);
		System.arraycopy(this.bidsLeft[opponent], 0, result.bidsLeft[1], 0, this.bidsLeft[opponent].length);

		assert(result.score.length == 2);
		/* A hack with reseting the scores to zero for an easier debugging.
		result.score[0] = this.score[this.id];
		result.score[1] = this.score[opponent];
		*/

		result.deck = (int[]) this.deck.clone();

		return result;
	}
}
