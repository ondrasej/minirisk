/*
A focused pesimistic minirisk player.
Copyright (C) 2009  Jiri Isa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package cz.matfyz.minirisk.clients.isa.jiri.afp;

import java.util.Random;
import cz.matfyz.minirisk.client.Client;

/** This is a focused pesimist minirisk player.
  * It expects the worst from a single oponent and plays against it.
  * @author Jiri Isa
  */
public class AFocusedPessimistClient extends Client {
	GameState state = null;
	static Random rnd = new Random(42);
	//static Marvin marvin = new Marvin(rnd);
	//static Marvin marvin = new McMarvin(rnd);
	static Marvin marvin = new VonMarvin(rnd);
	int[] prevBids = null;

	/** Creates a new instance of the player.
	  * @throws Exception In case of the connection problem.
	  */
	public AFocusedPessimistClient() throws Exception {
		super("AFocusedPessimistClient/" + marvin.getName());
	}

	/** Creates a new instance of the player.
	  * @param hostname Game server host name.
	  * @param port Game server port.
	  * @throws Exception In case of the connection problem.
	  */
	public AFocusedPessimistClient(String hostname, int port) throws Exception {
		super(hostname, port, "AFocusedPessimistClient/" + marvin.getName());
	}

	/** This function is invoked when the game round start. */
	protected void start() {
		state = new GameState(this.getPlayersCount(), this.getID());
	}

	/** The function invoked to react to the cards on the table.
	  * If there are more cards on the table, only the last one (the new one) is taken into account,
	  * because the reactions to the other cards were already played.
	  */
	public int play(int[] cards) {
		state.setDeck(cards);
		return marvin.advise(this.state);
	}

	/** This function informs the player about the situation on the table.
	  * After the central card is placed on the table, this function informs about all the reactions.
	  * This player's reaction is on the index equal to this player's ID.
	  * @param values The values offered by all players.
	  * @see getID
	  */
	protected void bids(int[] values) {
		this.prevBids = (int[]) values.clone();
	}

	/** This function is invoked to inform the player about how much everyone has won in the game step.
	  * @param value The amount of points won in the last round.
	  */
	protected void gains(int[] values) {
		state.update(this.prevBids, values);
		return;
	};

	/** Runs the player.
	  * @param args The command line parameters.
	  * <ul>
	  *   <li>The first one could be the game server hostname. By default "localhost".</li>
	  *   <li>The second one could be the game server port. By default 7654.</li>
	  * </ul>
	  * If any parameter is to be used, all preceding ones have to be specified.
	  */
	public static void main(String[] args) throws Exception {
		String hostname = (args.length > 0 ? args[0] : DEFAULT_HOSTNAME);
		int port = (args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_PORT);

		Client client = new AFocusedPessimistClient(hostname, port);
		client.run();
	}
}
