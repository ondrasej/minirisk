/*
The simple reinforcemet-learning minirisk player implementation.
Copyright (C) 2006  Jiri Isa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package cz.matfyz.minirisk.clients.isa.jiri.simpleRL;

import cz.matfyz.minirisk.client.Client;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.BitSet;
import java.util.Random;

/** This minirisk player creates a mapping "deck_card x bid_card -&lt; expected_utility"
  * based on its own experience. This is achieved using a very simplified version of a reinforcement
  * learning algorithm.
  */
public class SimpleRLClient extends Client {
	/** Number of cards in the game. Both the deck cards and the bid cards. */
	private final static int CARDS_COUNT = 15;

	/** The minimum value of a deck card. */
	private final static int MIN_CARD = -5;

	/** The maximum value of a deck card */
	private final static int MAX_CARD = 10;

	/** The exploration parameter. Exponentially decreasing.
	  * It determines a ratio of the non-otimal actions.
	  */
	private double exploration = 120;

	/** The decrease speed of the exploration parameter.
	  * A multiplicative constant. Applied after every game.
	  */
	private final static double EXPLORATION_DECREASE = 0.98;

	/** The minimal exploration value.
	  * Used to avoid arithymetic overflows.
	  */
	private double explorationThreshold = 1.0;

	/** The expected utility is computed as a floating average
	  * of the observations.
	  * This method results in "forgetting". This parameter
	  * specifies the "strength" of the past average relative
	  * to the strength of a new observation (with the strength
	  * <code>(1 - AVG_WINDOW)</code>.
	  */
	private final static double AVG_WINDOW = 0.7;

	/** A profit in a single game. */
	int profit = 0;

	/** Sum of all single-game profits. */
	int totalProfit = 0;

	/** The current round in a game. */
	int round = -1;

	/** The mapping from deck cards and bid cards to the expected utility
	  * of playing a bid card for the given deck card.
	  */
	double[][] strategy = new double[CARDS_COUNT + 1][CARDS_COUNT]; //deck_cards_sum x bid_cards

	/** History of deck cards as played in the current game. */
	int[] historyCards = new int[CARDS_COUNT];

	/** History of the own actions as played in the current game. */
	int[] historyActions = new int[CARDS_COUNT];

	/** The cards already played (to check not to play the same again).
	  * Corresponds to historyActions, but is easier and faster to work with.
	  * The bit i is set to true when the card i has already been played by this player.
	  */
	BitSet alreadyPlayed = new BitSet(CARDS_COUNT);

	/** The source of randomness. */
	Random rnd  = new Random();

	/** Creates a new instance of the player.
	  * Connects to the default host and default port.
	  * @throws UnknownHostException Cannot find the default host.
	  * @throws IOException Connection initialization error.
	  */
	public SimpleRLClient() throws UnknownHostException, IOException {
		super("SimpleRLClient");
	}

	/** Creates a new instance of this player.
	  * @param hostname The minirisk game server host name.
	  * @param port The minirisk game server port number.
	  * @throws UnknownHostException Cannot find the given host.
	  * @throws IOException Connection initialization error.
	  */
	public SimpleRLClient(String hostname, int port) throws UnknownHostException, IOException {
		super(hostname, port, "SimpleRLClient");
	}

	/** This function is invoked when the tournament is over.
	  */
	protected void quit() {
		for(int i = 0; i < strategy.length; i++) {
			System.out.print(i + MIN_CARD);
			System.out.print(":\t");

			for(int j = 0; j < strategy[i].length; j++) {
				System.out.print(strategy[i][j]);
				System.out.print(' ');
			}
			System.out.println();
		}
		System.out.println("I am requested to quit. I won " + totalProfit + " points in total.");
	}

	/** This function is invoked when a new game starts. */
	protected void start() {
		//System.out.println("A new game starts.");
		round = -1;
		profit = 0;
		alreadyPlayed.clear();
	}

	/** This function is invoked when a game s over. */
	protected void end() {
		learn();
		exploration *= EXPLORATION_DECREASE;
		if(exploration < explorationThreshold) {
			exploration = explorationThreshold;
		}

		totalProfit += profit;

		System.out.println("A game just ended and I won " + profit + " points.");
	}

	/** This function is called to decide what to play.
	  * It uses the strategy table. The action is selected
	  * among the allowed actions using the Boltzmann distribution.
	  * The selection become exponentially more strict with time,
	  * thus resulting in higher exploitation and lower exploration.
	  * @param cards The deck cards on the table.
	  */
	protected int play(int[] cards) {
		round++;

		//This is a SIMPLE player -> it cannot handle more cards then one.
		//So if their values sum is inside a range, it is going to be considered a single card. Otherwise it is truncated.
		int value = 0;
		for(int i = 0; i < cards.length; i++) {
			value += cards[i];
		}

		value = Math.min(value, MAX_CARD);
		value = Math.max(MIN_CARD, value);
		value -= MIN_CARD; //zero-based

		double sum = 0;
		for(int i = 0; i < CARDS_COUNT; i++) {
			if(alreadyPlayed.get(i)) {
				continue;
			}


			double val = Math.exp(strategy[value][i] / exploration);
			//System.out.print(val);
			//System.out.print(' ');
			sum += val;
		}
		//System.out.println();

		double rn = rnd.nextDouble() * sum;
		int bestStrategy = 0;
		//System.out.println("Taking " + rn + " out of " + sum);

		for(int i = 0; i < CARDS_COUNT; i++) {
			if(alreadyPlayed.get(i)) {
				continue;
			}

			rn -= Math.exp(strategy[value][i] / exploration);
			if(rn <= 0) {
				bestStrategy = i;
				break;
			}
		}

		historyCards[round] = value;
		historyActions[round] = bestStrategy;
		alreadyPlayed.set(bestStrategy);

		return bestStrategy + 1;
	}

	/** This function is invoked to let the player know the last round gains. */
	protected void gains(int[] values) {
		int value = values[getID()];
		//System.out.println("I just received " + value + " points.");
		profit += value;
	}

	/** This function adapts the expected utility table with respect to the last game.
	  */
	protected void learn() {
		for(int i = 0; i < CARDS_COUNT; i++) {
			int card = historyCards[i];
			int action = historyActions[i];

			double normalizedProfit = profit * getPlayersCount(); //to compensate for a lower expected profit when there are more players

			strategy[card][action] = AVG_WINDOW * strategy[card][action] + (1 - AVG_WINDOW) * normalizedProfit;
		}
	}

	/** Starts the player. */
	public static void main(String args[]) throws UnknownHostException, IOException {
		Client client = null;
		
		switch(args.length) {
			case 0:
				client = new SimpleRLClient();
				break;
			case 1:
				client = new SimpleRLClient(args[0], DEFAULT_PORT);
				break;
			case 2: client = new SimpleRLClient(args[0], Integer.parseInt(args[1]));
				break;
			default:
				System.err.println("Too many parameters.");
				System.exit(1);
		}

		client.run();
	}
}
