package cz.matfyz.minirisk.clients.sykora.ondrej;

import java.io.*;

import cz.matfyz.minirisk.client.*;
import cz.matfyz.minirisk.server.Server;

/**
 *  MiniRisk client that uses a monte-carlo planning algorithm for
 *  estimating best actions. For opponent modelling it remembers
 *  all actions of the opponents and uses them to create approximate
 *  probabilities of opponent actions (which are then used to
 *  navigate the monte-carlo algorithm).
 *  
 * @author Ondra Sykora [ondrasej@centrum.cz]
 */
public class MemoryMCClient extends Client {
	
	/**
	 * Keeps track of the activity of a single user. Calculates his/her
	 * current score and his/her cards on hand and previous bids.
	 * 
	 * @author Ondra Sykora [ondrasej@centrum.cz]
	 */
	private class ClientTracker {
		/**
		 * The number by which the value for a bid in the {@link #previousBids}
		 * is increased on a bid. This value is greater than one so that the
		 * differences between the used and unused cards grow faster.
		 */
		public static final int BIDS_INCREASE = 5;
		
		public static final double LEARNING_PARAMETER = 0.1;
		
		/**
		 * List of flags corresponding to player's cards on hand. For each
		 * card there is <code>true</code> if the user has not used this card
		 * in the current game or <code>false</code> if the user has already
		 * used this card in the current game.
		 * 
		 * @see 	#isAvailableCard(int)
		 */
		private boolean[] cardsOnHand;
		
		/**
		 * Holds information about previous bids of this player. Each element
		 * of the <code>previousBids</code> array represents a deck card and
		 * keeps an array that stores for each currency card, how many times
		 * this client used the currency card to bid for the deck card.
		 * <p>
		 * For example to find out how many times the user used the currency
		 * card "14" to get the deck card "10", you should refer to
		 * <code>previousBids[10+5][14]</code>. The <code>+5</code> part of
		 * the first index is necessary to handle negative values of the deck
		 * cards.
		 * <p>
		 * The values in this array are initialized to one, so that each possible
		 * bid gets at least some probability in the beginning.
		 * 
		 * @see 	#getProbableReaction(int)
		 * @see 	#getPreviousBid(int, int)
		 */
		private double previousBids[][];
		
		/**
		 * Holds the probabilities of the bids for each deck card. These bids
		 * are updated only when {@link #previousBids} are updated to avoid
		 * unnecessary calculations.
		 * 
		 * @see #getBidsProbability(int)
		 */
		private double bidsProbability[][];
		
		/**
		 * Notifies the client tracker about a bid that the player made for
		 * a specified card. Updates the tracker's memory and list of available
		 * cards for the client.
		 * 
		 * @param deck		the deck card the player was responding to.
		 * @param currency	the currency card the client responded with.
		 */
		public void addPreviousBid(int deck, int currency) {
			deck += 15;
			if(deck < 0) {
				System.err.println("deck is out of range: " + (deck - 15));
				deck = 0;
			}
			if(deck >= 80)
				deck = 79;

			/*for(int i = 0; i < 16; i++) {
				previousBids[deck][i] *= (1.0 - LEARNING_PARAMETER);
			}
			
			previousBids[deck][currency] = 
					previousBids[deck][currency] + LEARNING_PARAMETER;*/
			previousBids[deck][currency] += BIDS_INCREASE;
			cardsOnHand[currency] = false;
			
			double count = 0;
			for(int i = 0; i < 16; i++)
				count = count + previousBids[deck][i];
			for(int i = 0; i < 16; i++)
				bidsProbability[deck][i] = (double)previousBids[deck][i] / count;
		}
		
		/**
		 *  Gets the probability distribution of bids for a specified deck card.
		 *  It does not remove cards that the player does not have on hand from
		 *  the distribution.
		 *  
		 * @param deck		the deck card the palyer is responding to.
		 * @return			an array containing the probability distributions
		 * 					describing the currency card that the player would use.
		 */
		public double[] getBidsProbability(int deck) {
			deck += 15;
			if(deck < 0) {
				System.err.println("deck is out of range: " + (deck - 15));
				deck = 0;
			}
			if(deck >= 80)
				deck = 79;
			return bidsProbability[deck];
		}
		
		/**
		 * Tests if the player can use a specified currency card.
		 * 
		 * @param card		the tested currency card
		 * @return			<code>true</code> if the player has still
		 * 					currency card <code>card</code> on hand.
		 */
		public boolean isAvailableCard(int card) {
			return cardsOnHand[card];
		}
		
		/**
		 * Resets the client tracker for a new game. Does not reset
		 * the memory containing previous bids of the player.
		 */
		public void reset() {
			cardsOnHand[0] = false;
			for(int i = 1; i < 16; i++)
				cardsOnHand[i] = true;
		}

		/**
		 * Creates a new instance of the client tracker and prepares it
		 * for a game.
		 */
		public ClientTracker() {
			cardsOnHand = new boolean[16];
			previousBids = new double[80][];
			bidsProbability = new double[80][];
			for(int i = 0; i < previousBids.length; i++) {
				previousBids[i] = new double[16];
				bidsProbability[i] = new double[16];
				for(int j = 0; j < previousBids[i].length; j++) {
					previousBids[i][j] = 1;
					bidsProbability[i][j] = 1.0 / 15.0;
				}
			}
			reset();
		}
	}
	
	/**
	 * The number of games tried for a single simulation in the monte-carlo
	 * algorithm.
	 * 
	 * @see #simulate(int, int[])
	 */
	int STEPS_PER_SIMULATION = 200;

	/**
	 * List of client trackers for all players (including this one). The
	 * index of this player can be obtained via {@link #getID()}.
	 */
	protected ClientTracker[] players;
	
	/**
	 * The deck card that is auctioned in the current turn. This value is updated
	 * whenever {@link #play(int[])} is called.
	 */
	int currentDeckCard = 0;
	/**
	 * Number of deck cards that where auctioned in the last turn. This field, along
	 * with the {@link #previousDeckCardCount} and {@link #previousDeckCards} are
	 * used for recording the current state of the game for the monte-carlo simulation.
	 * @see #previousDeckCardCount
	 * @see #previousDeckCards
	 */
	int lastCardCount = 0;
	/**
	 * The deck cards that were already actioned in the current game.
	 * @see #lastCardCount
	 * @see #previousDeckCards
	 */
	int[] previousDeckCards = new int[15];
	/**
	 * The number of valid entries in the {@link #previousDeckCards} array.
	 */
	int previousDeckCardCount = 0;
	
	int[] previousBids = new int[15];
	
	int previousBidsCount = 0;
	
	/**
	 * Notifies the client about bids of other players. Updates the memory
	 * of client trackers.
	 */
	protected void bids(int[] values) {
		int winner = Server.determineWinner(currentDeckCard, values);
		for(int player = 0; player < values.length; player++)
			players[player].addPreviousBid(currentDeckCard, values[player]);
		if(-1 == winner)
			previousDeckCardCount -= lastCardCount;
	}

	/**
	 * Helper class used to return results of the simulations run in
	 * its own thread in the {@link MemoryMCClient#play(int[])} method..
	 * 
	 * @author Ondra Sykora [ondrasej@centrum.cz]
	 * 
	 * @see		MemoryMCClient#play(int[])
	 */
	private class Results {
		/**
		 * Recieves the best score discovered by the simulation.
		 */
		public long score = Long.MIN_VALUE;
		/**
		 * Recieves the best bid discovered by the simulation (this bid
		 * resulted in the best score stored in {@link Results#score}.
		 */
		public int card;
	}

	/**
	 * Decides which currency card to use for bid for specified deck cards.
	 * Simulates the result of all possible currency cards using memories
	 * of client trackers and chooses the deck card with best results.
	 * 
	 * @param cards		the deck cards that are auctioned.
	 */
	@Override
	public int play(final int[] cards) {
		// treat multiple cards as sum of their values. use the range values
		// if the sum out of range [-5; 10]
		currentDeckCard = 0;
		for(int card : cards)
			currentDeckCard += card;
		
		lastCardCount = cards.length;
		System.out.println("Playing for: " + currentDeckCard);
		
		final ClientTracker thisPlayer = players[getID()];

		// run the simulation in two threads ("dual-core optimization")
		// the results from the threads are later merged into a single bid
		final Results res1 = new Results();
		final Results res2 = new Results();
		
		// TODO: refactor this
		Thread first = new Thread(new Runnable() {
			public void run() {
				res1.card = 1;
				res1.score = Long.MIN_VALUE;
				for(int card = 1; card < 8; card++) {
					if(!thisPlayer.isAvailableCard(card))
						continue;
					long gain = simulate(card, cards);
					System.out.println("Simulation for " + card + " gained " + gain);
					if(gain > res1.score) {
						res1.card = card;
						res1.score = gain;
					}
				}
			}
		});
		Thread second = new Thread(new Runnable() {
			public void run() {
				res2.card = 8;
				res2.score = Long.MIN_VALUE;
				for(int card = 8; card < 16; card++) {
					if(!thisPlayer.isAvailableCard(card))
						continue;
					long gain = simulate(card, cards);
					System.out.println("Simulation for " + card + " gained " + gain);
					if(gain > res2.score) {
						res2.card = card;
						res2.score = gain;
					}
				}
			}
		});
		first.start();
		second.start();
		try {
			first.join();
			second.join();
		}
		catch(Exception err) {
			err.printStackTrace();
		}

		int best_card = res1.score > res2.score ? res1.card : res2.card;
		if(!thisPlayer.isAvailableCard(best_card)) {
			// The best card is not available... try res1 and res2 cards
			if(thisPlayer.isAvailableCard(res1.card))
				best_card = res1.card;
			else if(thisPlayer.isAvailableCard(res2.card))
				best_card = res2.card;
			else {
				for(int card = 1; card < 16; card++) {
					if(thisPlayer.isAvailableCard(card)) {
						best_card = card;
						break;
					}
				}
			}
		}
		System.out.println("MC playing: " + best_card);
		
		for(int card : cards)
			previousDeckCards[previousDeckCardCount++] = card;
		return best_card;
	}
	
	/**
	 * Simulates the effects of using currency card <code>card</code>
	 * and returns the expected gain.
	 * 
	 * @param evaluated_card	the currency card whose effect is simulated
	 * @return					the expected gain for the currency card.
	 */
	public long simulate(int evaluated_card, int[] current_deck_cards) {
		int bids[] = new int[getPlayersCount()];
		double scores[] = new double[getPlayersCount()];
		
		int[] deck_cards = new int[15];
		for(int i = 0; i < deck_cards.length; i++)
			deck_cards[i] = i < 5 ? -i - 1 : i - 4;

		for(int i = 0; i < previousDeckCardCount; i++)
			for(int cp = i; cp < deck_cards.length; cp++)
				if(previousDeckCards[i] == deck_cards[cp]) {
					ClientUtils.swapArrayElements(deck_cards, i, cp);
					break;
				}
		for(int i = 0; i < previousDeckCardCount + current_deck_cards.length; i++)
			for(int cp = i; cp < deck_cards.length; cp++)
				if(previousDeckCards[i] == deck_cards[cp]) {
					ClientUtils.swapArrayElements(deck_cards, i, cp);
					break;
				}
		
		boolean[][] available_cards = new boolean[getPlayersCount()][];
		for(int i = 0; i < available_cards.length; i++)
			available_cards[i] = new boolean[16];
		
		for(int game = 1; game < STEPS_PER_SIMULATION; game++) {
			// prepare a random game
			ClientUtils.randomizeArray(deck_cards, previousDeckCardCount + current_deck_cards.length, deck_cards.length);
			for(int player = 0; player < available_cards.length; player++)
				for(int card = 0; card < available_cards[player].length; card++)
					available_cards[player][card] = players[player].isAvailableCard(card);
			
			int card = 0;
			for(int c : current_deck_cards)
				card += c;
			final int start_step = previousDeckCardCount + current_deck_cards.length - 1;
			for(int step = start_step; step < deck_cards.length; step++) {		
				for(int player = 0; player < getPlayersCount(); player++) {
					double[] bids_probabilities = players[player].getBidsProbability(card);
					bids[player] = ClientUtils.chooseByProbability(bids_probabilities, available_cards[player]);
				}
				if(step == start_step)
					bids[getID()] = evaluated_card;
				
				for(int i = 0; i < getPlayersCount(); i++)
					available_cards[i][bids[i]] = false;
				
				int winner = Server.determineWinner(card, bids);
				if(winner >= 0) {
					scores[winner] += (double)card;
					card = 0;
				}
				if(step < deck_cards.length - 1)
					card += deck_cards[step + 1];
			}
		}
		double others_scores = 0.0;
		for(int i = 0; i < getPlayersCount(); i++)
			if(i != getID())
				others_scores += scores[i];
		
		return (long)(scores[getID()] - others_scores);
	}
	
	/**
	 * Resets the client to start a new game. Allocates new client trackers
	 * if necessary (or if the number of players has changed) and resets
	 * all client trackers for a new game.
	 */
	@Override
	protected void start() {
		super.start();
		if(players == null || players.length != getPlayersCount()) {
			players = new ClientTracker[getPlayersCount()];
			for(int i = 0; i < getPlayersCount(); i++)
				players[i] = new ClientTracker();
		}
		for(int i = 0; i < getPlayersCount(); i++)
			players[i].reset();
		previousDeckCardCount = 0;
		previousBidsCount = 0;
	}

	/**
	 * Creates a new client and connects to a server at specified address
	 * and port.
	 * 
	 * @param hostName			host name of the game server.
	 * @param port				port that is used by the server.
	 * @param nickName			name of this player.
	 * @throws IOException		if the client could not connect to
	 * 							the server.
	 */
	public MemoryMCClient(String hostName, int port, String nickName) throws IOException {
		super(hostName, port, nickName);
	}
	
	/**
	 * Creates a new client and connects to the default hostname and port.
	 * 
	 * @param nickName			name of this player.
	 * @throws IOException		if the client could not connect to
	 * 							the server.
	 */
	public MemoryMCClient(String nickName) throws IOException {
		super(nickName);
	}
	
	/**
	 * The entry point for this client. Creates a new client and connects
	 * to the server specified by command-line parameters (or to the default
	 * hostname/port if connection parameters are not specified on the command
	 * line).
	 * 
	 * @param args			command line parameters.
	 * @throws Exception	if something unexpected happens.
	 */
	public static void main(String[] args) throws Exception {
		String hostname = (args.length > 1 ? args[1] : DEFAULT_HOSTNAME);
		int port = (args.length > 2 ? Integer.parseInt(args[2]) : DEFAULT_PORT);

		Client client = new MemoryMCClient(hostname, port, "MemoryMCClient");
		client.run();
	}
};
