package cz.matfyz.minirisk.clients.sykora.ondrej;

import java.io.*;
import cz.matfyz.minirisk.client.*;
import cz.matfyz.minirisk.server.Server;

public class MemoryClient extends Client implements Cloneable {
	
	/**
	 * Keeps track of the activity of a single user. Calculates his/her
	 * current score and his/her cards on hand and previous bids.
	 * 
	 * @author Ondra Sykora [ondrasej@centrum.cz]
	 */
	private class ClientTracker {
		/**
		 * List of flags corresponding to player's cards on hand. For each
		 * card there is <code>true</code> if the user has not used this card
		 * in the current game or <code>false</code> if the user has already
		 * used this card in the current game.
		 * 
		 * @see 	#isAvailableCard(int)
		 */
		private boolean[] cardsOnHand;

		/**
		 * The score of the player in the current game.
		 * 
		 * @see		#getScore()
		 */
		private int score;
		
		/**
		 * Holds information about previous bids of this player. Each element
		 * of the <code>previousBids</code> array represents a deck card and
		 * keeps an array that stores for each currency card, how many times
		 * this client used the currency card to bid for the deck card.
		 * <p>
		 * For example to find out how many times the user used the currency
		 * card "14" to get the deck card "10", you should refer to
		 * <code>previousBids[10+5][14]</code>. The <code>+5</code> part of
		 * the first index is necessary to handle negative values of the deck
		 * cards. 
		 * 
		 * @see 	#getProbableReaction(int)
		 * @see 	#getPreviousBid(int, int)
		 */
		private int previousBids[][];
		
		/**
		 * Notifies the client tracker about a bid that the player made for
		 * a specified card. Updates the tracker's memory and list of available
		 * cards for the client.
		 * 
		 * @param deck		the deck card the player was responding to.
		 * @param currency	the currency card the client responded with.
		 */
		public void addPreviousBid(int deck, int currency) {
			previousBids[deck + 5][currency]++;
			cardsOnHand[currency] = false;
		}
		
		/**
		 * Creates a new copy of the client tracker objects and copies all
		 * internal data.
		 * 
		 * @return			deep copy of this object.
		 */
		public ClientTracker clone() {
			ClientTracker copy = new ClientTracker();
			copy.score = this.score;
			for(int i = 0; i < 16; i++)
				copy.cardsOnHand[i] = this.cardsOnHand[i];
			for(int i = 0; i < 16; i++)
				for (int j = 0; j < 16; j++)
					copy.previousBids[i][j] = this.previousBids[i][j];
			
			return copy;
		}
		
		/**
		 * Gets the number of auctions where the player responded with
		 * <code>currency</code> card to <code>deck</code> card.
		 * 
		 * @param deck		the deck card the player was responding to
		 * @param currency	the currency card the player responded with
		 * @return			number of auctions where the player responded
		 * 					with <code>currency</code> to <code>deck</code>.
		 */
		public int getPreviousBid(int deck, int currency) {
			return previousBids[deck + 5][currency];
		}
		
		/**
		 * Gets the most probable played currency card that the player will
		 * use given the deck card that is currently on the table and cards
		 * that the player has on hand.
		 * <p>
		 * If the client tracker has not enough information it returns any
		 * card the player has on hand, starting with middle cards
		 * (7, 6, 8, 5, 9, ...)
		 * 
		 * @param deck		the deck card that is on the table.
		 * @return			the most probable reaction of the player based on
		 * 					his previous bids on the same deck card.
		 */
		public int getProbableReaction(int deck) {
			int max = -1;
			int max_card = 0;
			// try to find player's best bid between the cards he can use so far
			for(int currency = 1; currency <= 15; currency++) {
				if(cardsOnHand[currency]) {
					int c = getPreviousBid(deck, currency);
					if(c > max) {
						max_card = currency;
						max = c;
					}
				}
			}
			// if no card was chosen, select one that was not used yet, starting with
			// "7" and going to the edges
			if(max == 0) {
				for(int c = 0; c <= 7; c++) {
					if(cardsOnHand[7 - c])
						return 7 - c;
					else if(cardsOnHand[7 + c])
						return 7 + c;
				}
			}
			return max_card;
		}
		
		/**
		 * Returns the current score of the player.
		 * <p>
		 * This is not supported at the moment.
		 * 
		 * @return			the current score of the player.
		 */
		public int getScore() {
			return score;
		}

		/**
		 * Tests if the player can use a specified currency card.
		 * 
		 * @param card		the tested currency card
		 * @return			<code>true</code> if the player has still
		 * 					currency card <code>card</code> on hand.
		 */
		public boolean isAvailableCard(int card) {
			return cardsOnHand[card];
		}
		
		/**
		 * Resets the client tracker for a new game. Does not reset
		 * the memory containing previous bids of the player.
		 */
		public void reset() {
			score = 0;
			for(int i = 1; i < 16; i++)
				cardsOnHand[i] = true;
		}

		/**
		 * Creates a new instance of the client tracker and prepares it
		 * for a game.
		 */
		public ClientTracker() {
			cardsOnHand = new boolean[16];
			previousBids = new int[16][];
			for(int i = 0; i < previousBids.length; i++)
				previousBids[i] = new int[16];
			reset();
		}
	}

	/**
	 * List of client trackers for all players (including this one). The
	 * index of this player can be obtained via {@link #getID()}.
	 */
	protected ClientTracker[] players;
	
	/**
	 * The deck card that is auctioned in the current turn. This value is updated
	 * whenever {@link #play(int[])} is called.
	 */
	int currentDeckCard = 0;
	
	/**
	 * Notifies the client about bids of other players. Updates the memory
	 * of client trackers.
	 */
	protected void bids(int[] values) {
		for(int player = 0; player < values.length; player++)
			players[player].addPreviousBid(currentDeckCard, values[player]);
	}

	/**
	 * Decides which currency card to use for bid for specified deck cards.
	 * Simulates the result of all possible currency cards using memories
	 * of client trackers and chooses the deck card with best results.
	 * 
	 * @param cards		the deck cards that are auctioned.
	 */
	@Override
	public int play(int[] cards) {
		// treat multiple cards as sum of their values. use the range values
		// if the sum out of range [-5; 10]
		currentDeckCard = 0;
		for(int card : cards)
			currentDeckCard += card;
		if(currentDeckCard < -5)
			currentDeckCard = -5;
		if(currentDeckCard > 10)
			currentDeckCard = 10;
		
		int zero_card = 0;
		for(int card = 1; card < 16; card++) {
			if(!players[getID()].isAvailableCard(card))
				continue;
			int gain = simulate(card);
			if(gain > 0) {
				System.out.println("MC playing: " + card);
				return card;
			}
			else if (gain == 0 && zero_card == 0)
				zero_card = card;
		}
		if(zero_card == 0) {
			// no card with zero effect was chosen, then just select one card
			// from hand
			System.out.println("MC: no zero card");
			zero_card = players[getID()].getProbableReaction(currentDeckCard);
		}
		System.out.println("MC playing: " + zero_card);
		return zero_card;
	}
	
	/**
	 * Simulates the effects of using currency card <code>card</code>
	 * and returns the expected gain.
	 * 
	 * @param card	the currency card whose effect is simulated
	 * @return		the expected gain for the currency card.
	 */
	public int simulate(int card) {
		int[] bids = new int[players.length];
		for(int player = 0; player < players.length; player++) {
			if(player != getID())
				bids[player] = players[player].getProbableReaction(currentDeckCard);
			else
				bids[player] = card;
		}
		int winner = Server.determineWinner(currentDeckCard, bids);
		if(winner == getID())
			return currentDeckCard;
		return 0;
	}
	
	/**
	 * Resets the client to start a new game. Allocates new client trackers
	 * if necessary (or if the number of players has changed) and resets
	 * all client trackers for a new game.
	 */
	@Override
	protected void start() {
		super.start();
		if(players == null || players.length != getPlayersCount()) {
			players = new ClientTracker[getPlayersCount()];
			for(int i = 0; i < getPlayersCount(); i++)
				players[i] = new ClientTracker();
		}
		for(int i = 0; i < getPlayersCount(); i++)
			players[i].reset();
	}

	/**
	 * Creates a new client and connects to a server at specified address
	 * and port.
	 * 
	 * @param hostName			host name of the game server.
	 * @param port				port that is used by the server.
	 * @param nickName			name of this player.
	 * @throws IOException		if the client could not connect to
	 * 							the server.
	 */
	public MemoryClient(String hostName, int port, String nickName) throws IOException {
		super(hostName, port, nickName);
	}
	
	/**
	 * Creates a new client and connects to the default hostname and port.
	 * 
	 * @param nickName			name of this player.
	 * @throws IOException		if the client could not connect to
	 * 							the server.
	 */
	public MemoryClient(String nickName) throws IOException {
		super(nickName);
	}
	
	/**
	 * The entry point for this client. Creates a new client and connects
	 * to the server specified by command-line parameters (or to the default
	 * hostname/port if connection parameters are not specified on the command
	 * line).
	 * 
	 * @param args			command line parameters.
	 * @throws Exception	if something unexpected happens.
	 */
	public static void main(String[] args) throws Exception {
		String hostname = (args.length > 0 ? args[0] : DEFAULT_HOSTNAME);
		int port = (args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_PORT);

		Client client = new MemoryClient(hostname, port, "MemoryClient");
		client.run();
	}
};
