package cz.matfyz.minirisk.clients.sykora.ondrej;

import java.util.*;

/**
 * Contains some helper functions that are too generic to be placed in one
 * of the clients.
 * 
 * @author Ondra Sykora [ondrasej@centrum.cz]
 */
public final class ClientUtils {
	/**
	 * Random number generator used by some of the functions.
	 */
	public static final Random randomGenerator = new Random(123456789);
	
	/**
	 *  Chooses one element of the arrays based on its probability. The first array
	 *  contains probability distribution for the elements and the second contains
	 *  information about availability of the items - items that are not available
	 *  are not considered during the selection process.
	 *  
	 *  The function expects that both arrays represent the same data and thus have
	 *  the same number of elements.
	 *  
	 * @param probabilities			probabilities of the elements. They shoudl be
	 * 								normalized to 1.
	 * @param availability			<code>true</code> for elements that may be chosen
	 * 								by the method, <code>false</code> for those that
	 * 								should be skipped.
	 * @return						index of the selected element.
	 */
	public static int chooseByProbability(double[] probabilities, boolean[] availability) {
		assert probabilities != null : "probabilities array was not specified";
		assert availability != null : "availability array was not specified";
		assert probabilities.length == availability.length : "the probabilities and availability arrays have different lengths";
		
		double total_probability = 0.0;
		for(int i = 0; i < probabilities.length; i++)
			if(availability[i])
				total_probability += probabilities[i];
		
		assert total_probability <= 1.0 : "The sum of all probabilities exceeds one";
		if(total_probability <= 0.0) {
			for(int i = 0; i < availability.length; i++)
				if(availability[i])
					return i;
			throw new RuntimeException("There is no card available");
		}
		
		double chosen = randomGenerator.nextDouble() * total_probability;
		total_probability = 0.0;
		for(int i = 0; i < probabilities.length; i++)
			if(availability[i]) {
				total_probability += probabilities[i];
				if(total_probability >= chosen)
					return i;
			}
		throw new RuntimeException("No card was chosen");
	}

	/**
	 * Calculates the probability that the value chosen using probability distribution
	 * <code>probabilities</code> from the set of items specified by
	 * <code>availability</code> is below <code>value</code>.
	 * 
	 * @param value					the value whose probability of winning is calculated.
	 * @param probabilities			the probability distribution of values.
	 * @param availability			the specfication of elements that are available
	 * 								for the random choice.
	 * @return						the probability that the element chosen using the
	 * 								speicified probability distribution is below
	 * 								<code>value</code>
	 */
	public static double getProbabilityBelow(int value, double[] probabilities, boolean[] availability) {
		assert probabilities != null : "probabilities array was not specified";
		assert availability != null : "availaility array was not specified";
		assert probabilities.length == availability.length : "the probability and availability arrays have different lengths";
		assert value >= 0 && value < probabilities.length : "index of the requested card is out of bounds";
		
		double probability_total = 0.0;
		double probability_below = 0.0;
		for(int i = 0; i < probabilities.length; i++) {
			if(availability[i]) {
				probability_total += probabilities[i];
				if(i < value)
					probability_below += probabilities[i];
			}
		}
		assert probability_total > 0.0 : "the total probability is zero";
		return probability_below / probability_total;
	}
	
	/**
	 * Swaps elements in the array so that they are in random order.
	 * 
	 * @param array					the array whose elements are swaped.
	 * @param start					the index of the first element that can
	 * 								be moved by the function.
	 * @param end					the index of the first element that
	 * 								is not moved by the function.
	 */
	public static void randomizeArray(int[] array, int start, int end) {
		for(int i = start; i < end - 1; i++) {
			int tmp = array[i];
			int pos = i + randomGenerator.nextInt(array.length - i);
			array[i] = array[pos];
			array[pos] = tmp;
		}
	}
	
	/**
	 * Swaps two elements of an array of integers.
	 * 
	 * @param array					the array in which the elements are swapped.
	 * @param first					the index of the first element.
	 * @param second				the index of the second element.
	 */
	public static void swapArrayElements(int[] array, int first, int second) {
		int tmp = array[first];
		array[first] = array[second];
		array[second] = tmp;
	}
	
	/**
	 * Private constructor - prevents everyone from creating instances of the class. 
	 */
	private ClientUtils() {
		throw new RuntimeException("No instances!");
	}
}
