package cz.matfyz.minirisk.clients.sykora.ondrej;

import java.io.*;
import java.net.UnknownHostException;
import java.util.*;

import cz.matfyz.minirisk.client.*;

/**
 * The minirisk client that uses a fixed mixed strategy. Its strategy may
 * be either specified apriori or be randomly generated when the client
 * is created.
 * <b>
 * This client is intended mainly for testing of strengths of fixed mixed
 * strategies.
 * </b>
 * 
 * @author Ondra Sykora [ondrasej@centrum.cz]
 */
public class MixedStrategyClient extends cz.matfyz.minirisk.client.Client {

	/**
	 * The radnom number generator used for random strategy generating.
	 */
	private Random randomGenerator = new Random(System.currentTimeMillis());
	
	/**
	 * The currency cards that are currently available to the client.
	 * 
	 * @see		#play(int[])
	 */
	private boolean[] cardsOnHand = new boolean[15];
	
	/**
	 * The strategy that the client uses. The first index of the array is
	 * the sum of the current deck cards +15. The other index is the value
	 * of the currency card.
	 * <p>
	 * The value at <code>strategy[<i>i</i>][<i>j</i>]</code> is the
	 * probability of the currency <i>j</i> card being chosen when the
	 * current deck cards sum up to <i>i</i>.
	 * 
	 * @see 	#generateRandomStrategy()
	 * @see 	#play(int[])
	 */
	private double[][] strategy;
	
	/**
	 * Generates a random mixed strategy for the client.
	 */
	private void generateRandomStrategy() {
		strategy = new double[80][];
		for(int i = 0; i < strategy.length; i++) {
			strategy[i] = new double[15];
			double total = 0.0;
			for (int j = 0; j < strategy[i].length; j++) {
				strategy[i][j] = randomGenerator.nextDouble();
				total += strategy[i][j];
			}
			for(int j = 0; j < strategy[i].length; j++)
				strategy[i][j] /= total;
		}
	}
	
	/**
	 * Chooses a currency card to play in this turn.
	 * 
	 * @param cards			the current deck cards.
	 * @return				the currency card chosen.
	 */
	@Override
	protected int play(int[] cards) {
		int effective_card = 0;
		for(int card : cards)
			effective_card += card;
		int card = ClientUtils.chooseByProbability(strategy[effective_card + 15], cardsOnHand);
		cardsOnHand[card] = false;
		return card + 1;
	}
	
	/**
	 * Starts a new game. Resets the available currency cards.
	 */
	@Override
	protected void start() {
		super.start();
		for(int i = 0; i < cardsOnHand.length; i++)
			cardsOnHand[i] = true;
	}
	
	/**
	 * Handles the end of the tournament. Writes out the strategy
	 * the client used in the tournament.
	 */
	@Override
	protected void quit() {
		// TODO Auto-generated method stub
		super.quit();
		writeStrategy();
	}

	/**
	 * Writes the chosen strategy to standard output.
	 */
	protected void writeStrategy() {
		System.out.println("My strategy: ");
		for(int i = 0; i < strategy.length; i++) {
			System.out.print(i - 15);
			System.out.print(" ->");
			for(int j = 0; j < strategy[i].length; j++)
				System.out.print(" " + strategy[i][j]);
			System.out.println();
		}
	}

	/** Creates a new instance of the player with the given server hostname and port.
	  * @param hostname The game server hostname.
	  * @param port The game server port.
	  * @param nickname The identification string of this player.
	  * @throws UnknownHostException When the requested server hostname is unknown.
	  * @throws IOException Connection initialization failed.
	  */
	public MixedStrategyClient(String hostName, int port, String nickName) throws IOException {
		super(hostName, port, nickName);
		generateRandomStrategy();
	}
	
	/** Creates a new instance of the player with the default server hostname and port.
	  * The default hostname is "localhost" and the default port is 7654.
	  * @param nickname The identification string of this player.
	  * @throws UnknownHostException When the requested server hostname is unknown.
	  * @throws IOException Connection initialization failed.
	  */
	public MixedStrategyClient(String nickName) throws IOException {
		super(nickName);
		generateRandomStrategy();
	}
	
	/** Creates a new instance of the player with the default server hostname and port.
	  * The default hostname is "localhost" and the default port is 7654.
	  * @param strategy The mixed strategy the client will use.
	  * @param nickname The identification string of this player.
	  * @throws UnknownHostException When the requested server hostname is unknown.
	  * @throws IOException Connection initialization failed.
	  */
	public MixedStrategyClient(double[][] strategy, String nickname) throws IOException {
		super(nickname);
		this.strategy = strategy;
	}
	
	/**
	 * The entry point for this client. Creates a new client and connects
	 * to the server specified by command-line parameters (or to the default
	 * hostname/port if connection parameters are not specified on the command
	 * line).
	 * 
	 * @param args			command line parameters.
	 * @throws Exception	if something unexpected happens.
	 */
	public static void main(String[] args) throws Exception {
		String hostname = (args.length > 1 ? args[1] : DEFAULT_HOSTNAME);
		int port = (args.length > 2 ? Integer.parseInt(args[2]) : DEFAULT_PORT);

		Client client = new MixedStrategyClient(hostname, port, "MixedStrategyClient");
		client.run();
	}
}
