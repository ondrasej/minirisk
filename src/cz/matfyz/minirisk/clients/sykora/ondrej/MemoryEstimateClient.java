package cz.matfyz.minirisk.clients.sykora.ondrej;

import java.io.*;
import cz.matfyz.minirisk.client.*;
import cz.matfyz.minirisk.server.Server;

public class MemoryEstimateClient extends Client implements Cloneable {
	
	/**
	 * Keeps track of the activity of a single user. Calculates his/her
	 * current score and his/her cards on hand and previous bids.
	 * 
	 * @author Ondra Sykora [ondrasej@centrum.cz]
	 */
	private class ClientTracker {
		/**
		 * The number by which the value for a bid in the {@link #previousBids}
		 * is increased on a bid. This value is greater than one so that the
		 * differences between the used and unused cards grow faster.
		 */
		public static final int BIDS_INCREASE = 5;
		
		/**
		 * List of flags corresponding to player's cards on hand. For each
		 * card there is <code>true</code> if the user has not used this card
		 * in the current game or <code>false</code> if the user has already
		 * used this card in the current game.
		 * 
		 * @see 	#isAvailableCard(int)
		 */
		private boolean[] cardsOnHand;
		
		/**
		 * Holds information about previous bids of this player. Each element
		 * of the <code>previousBids</code> array represents a deck card and
		 * keeps an array that stores for each currency card, how many times
		 * this client used the currency card to bid for the deck card.
		 * <p>
		 * For example to find out how many times the user used the currency
		 * card "14" to get the deck card "10", you should refer to
		 * <code>previousBids[10+5][14]</code>. The <code>+5</code> part of
		 * the first index is necessary to handle negative values of the deck
		 * cards.
		 * <p>
		 * The values in this array are initialized to one, so that each possible
		 * bid gets at least some probability in the beginning.
		 * 
		 * @see 	#getProbableReaction(int)
		 * @see 	#getPreviousBid(int, int)
		 */
		private double previousBids[][];
		
		/**
		 * Holds the probabilities of the bids for each deck card. These bids
		 * are updated only when {@link #previousBids} are updated to avoid
		 * unnecessary calculations.
		 * 
		 * @see #getBidsProbability(int)
		 */
		private double bidsProbability[][];
		
		/**
		 * Notifies the client tracker about a bid that the player made for
		 * a specified card. Updates the tracker's memory and list of available
		 * cards for the client.
		 * 
		 * @param deck		the deck card the player was responding to.
		 * @param currency	the currency card the client responded with.
		 */
		public void addPreviousBid(int deck, int currency) {
			previousBids[deck + 15][currency] = 
					previousBids[deck + 15][currency] + BIDS_INCREASE;
			cardsOnHand[currency] = false;
			
			double count = 0;
			for(int i = 0; i < 16; i++)
				count = count + previousBids[deck + 15][i];
			for(int i = 0; i < 16; i++)
				bidsProbability[deck + 15][i] = (double)previousBids[deck + 15][i] / count;
		}
		
		/**
		 *  Gets the probability distribution of bids for a specified deck card.
		 *  It does not remove cards that the player does not have on hand from
		 *  the distribution.
		 *  
		 * @param deck		the deck card the palyer is responding to.
		 * @return			an array containing the probability distributions
		 * 					describing the currency card that the player would use.
		 */
		public double[] getBidsProbability(int deck) {
			return bidsProbability[deck + 15];
		}
		
		public boolean[] getCardsOnHand() {
			return cardsOnHand;
		}
		
		/**
		 * Tests if the player can use a specified currency card.
		 * 
		 * @param card		the tested currency card
		 * @return			<code>true</code> if the player has still
		 * 					currency card <code>card</code> on hand.
		 */
		public boolean isAvailableCard(int card) {
			return cardsOnHand[card];
		}
		
		/**
		 * Resets the client tracker for a new game. Does not reset
		 * the memory containing previous bids of the player.
		 */
		public void reset() {
			cardsOnHand[0] = false;
			for(int i = 1; i < 16; i++)
				cardsOnHand[i] = true;
		}

		/**
		 * Creates a new instance of the client tracker and prepares it
		 * for a game.
		 */
		public ClientTracker() {
			cardsOnHand = new boolean[16];
			previousBids = new double[80][];
			bidsProbability = new double[80][];
			for(int i = 0; i < previousBids.length; i++) {
				previousBids[i] = new double[16];
				bidsProbability[i] = new double[16];
				for(int j = 0; j < previousBids[i].length; j++) {
					previousBids[i][j] = 1;
					bidsProbability[i][j] = 1.0 / 15.0;
				}
			}
			reset();
		}
	}

	/**
	 * List of client trackers for all players (including this one). The
	 * index of this player can be obtained via {@link #getID()}.
	 */
	protected ClientTracker[] players;
	
	/**
	 * The deck card that is auctioned in the current turn. This value is updated
	 * whenever {@link #play(int[])} is called.
	 */
	int currentDeckCard = 0;
	
	/**
	 * Notifies the client about bids of other players. Updates the memory
	 * of client trackers.
	 */
	protected void bids(int[] values) {
		for(int player = 0; player < values.length; player++)
			players[player].addPreviousBid(currentDeckCard, values[player]);
	}

	/**
	 * Decides which currency card to use for bid for specified deck cards.
	 * Simulates the result of all possible currency cards using memories
	 * of client trackers and chooses the deck card with best results.
	 * 
	 * @param cards		the deck cards that are auctioned.
	 */
	@Override
	public int play(int[] cards) {
		// treat multiple cards as sum of their values. use the range values
		// if the sum out of range [-5; 10]
/*		currentDeckCard = 0;
		for(int card : cards)
			currentDeckCard += card;
		if(currentDeckCard < -5)
			currentDeckCard = -5;
		if(currentDeckCard > 10)
			currentDeckCard = 10;*/
		
		int max_card = 1;
		double max_value = -1;
		for(int card = 1; card < 16; card++) {
			if(!players[getID()].isAvailableCard(card))
				continue;
			double value = simulate(card) * currentDeckCard / card;
			System.out.println("Value for: " + card + " is: " + value);
			if(value > max_value) {
				max_card = card;
				max_value = value;
			}
		}
		return max_card;
	}
	
	/**
	 * Simulates the effects of using currency card <code>card</code>
	 * and returns the expected gain.
	 * 
	 * @param card	the currency card whose effect is simulated
	 * @return		the expected gain for the currency card.
	 */
	public double simulate(int card) {
		double min_probability = 1.0;
		for(int player = 0; player < players.length; player++) {
			double[] bids_probability = players[player].getBidsProbability(currentDeckCard);
			boolean[] available_cards = players[player].getCardsOnHand();
			
			double probability = ClientUtils.getProbabilityBelow(card, bids_probability, available_cards);
			min_probability = Math.min(min_probability, probability);
		}
		System.out.println("Simulation for: " + card + " has given: " + min_probability);
		return min_probability;
	}
	
	/**
	 * Resets the client to start a new game. Allocates new client trackers
	 * if necessary (or if the number of players has changed) and resets
	 * all client trackers for a new game.
	 */
	@Override
	protected void start() {
		super.start();
		if(players == null || players.length != getPlayersCount()) {
			players = new ClientTracker[getPlayersCount()];
			for(int i = 0; i < getPlayersCount(); i++)
				players[i] = new ClientTracker();
		}
		for(int i = 0; i < getPlayersCount(); i++)
			players[i].reset();
	}

	/**
	 * Creates a new client and connects to a server at specified address
	 * and port.
	 * 
	 * @param hostName			host name of the game server.
	 * @param port				port that is used by the server.
	 * @param nickName			name of this player.
	 * @throws IOException		if the client could not connect to
	 * 							the server.
	 */
	public MemoryEstimateClient(String hostName, int port, String nickName) throws IOException {
		super(hostName, port, nickName);
	}
	
	/**
	 * Creates a new client and connects to the default hostname and port.
	 * 
	 * @param nickName			name of this player.
	 * @throws IOException		if the client could not connect to
	 * 							the server.
	 */
	public MemoryEstimateClient(String nickName) throws IOException {
		super(nickName);
	}
	
	/**
	 * The entry point for this client. Creates a new client and connects
	 * to the server specified by command-line parameters (or to the default
	 * hostname/port if connection parameters are not specified on the command
	 * line).
	 * 
	 * @param args			command line parameters.
	 * @throws Exception	if something unexpected happens.
	 */
	public static void main(String[] args) throws Exception {
		String hostname = (args.length > 1 ? args[1] : DEFAULT_HOSTNAME);
		int port = (args.length > 2 ? Integer.parseInt(args[2]) : DEFAULT_PORT);

		Client client = new MemoryEstimateClient(hostname, port, "MemoryEstimateClient");
		client.run();
	}
};
