/*
A simple random-strategy minirisk player.
Copyright (C) 2006  Jiri Isa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package cz.matfyz.minirisk.clients.basic.random;

import cz.matfyz.minirisk.client.Client;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/** An example implementation of the minirisk player.
  * This player plays a random strategy.
  * @author Jiri Isa
  */
public class RandomClient extends Client {
	/** A list of unique actions.
	  * Each action is played in the corresponding round.
	  */
	ArrayList<Integer> strategy = new ArrayList<Integer>(15);

	/** The current round nuumber. */
	int round = 0;

	/** An accumulated profit obtained during a single game. */
	int profit = 0;

	/** The total accumulated profit. */
	int totalProfit = 0;

	/** The source of randomness. */
	Random rnd = new Random(10121);

	/** Creates a new instance of the player.
	  * Cnnects to the server on localhost and on the default port.
	  * @throws UnknownHostException When the hostname is not known.
	  * @throws IOException Input/output related problem.
	  */
	public RandomClient() throws UnknownHostException, IOException {
		super("RandomClient");
		initStrategy();
	}

	/** Creates a new instance of the player.
	  * Connects to the game server on the computer with the given hostname and listening on the given port.
	  * @param hostname Game server hostname.
	  * @param port Game server port.
	  * @throws UnknownHostException When the game server hostname is not nown.
	  * @throws IOException Input/output error.
	  */
	public RandomClient(String hostname, int port) throws UnknownHostException, IOException {
		super(hostname, port, "Random_client");
		initStrategy();
	}

	/** Initialilizes the strategy sequence.
	  * The allowed actions are placed into the strategy list,
	  * ensuring that no action is repeated during the game.
	  */
	private void initStrategy() {
		for(int i = 0; i < 15; i++) {
			strategy.add(new Integer(i + 1));
		}
	}

	/** When the tournament is over, this function is invoked.
	  */
	protected void quit() {
		System.out.println("I am requested to quit. I won " + totalProfit + " points in total.");
	}

	/** When the new game starts, this function is invoked.
	  */
	protected void start() {
		System.out.println("A new game starts.");
		Collections.shuffle(strategy, rnd);
		round = 0;
		profit = 0;
	}

	/** When the game ends, this function is invoked.
	  */
	protected void end() {
		totalProfit += profit;
		System.out.println("A game just ended and I won " + profit + " points.");
	}

	/** This function decides what to play in reaction to the given cards.
	  */
	protected int play(int[] cards) {
		int decision = strategy.get(round).intValue();
		round++;

		return decision;
	}

	/** This function receives all players' reactions to the last card on the table.
	  * @param values The reactions of all players. This player's reaction has the index
	  *               corresponding to this player's ID.
	  */
	protected void bids(int[] values) {
		System.out.print("Bids: ");
		for(int i = 0; i < values.length; i++) {
			System.out.print(values[i]);

			if(i != values.length - 1) {
				System.out.print(',');
			}
		}
		System.out.println();
	}

	/** This function is invoked to inform the player about the gains obtained in the last round.
	  * @param values The obtained points.
	  */
	protected void gains(int[] values) {
		int value = values[getID()];
		System.out.println("I just received " + value + " points.");
		profit += value;
	}

	public static void main(String args[]) throws UnknownHostException, IOException {
		String hostname = (args.length > 0 ? args[0] : DEFAULT_HOSTNAME);
		int port = (args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_PORT);

		Client client = new RandomClient(hostname, port);

		client.run();
	}
}
