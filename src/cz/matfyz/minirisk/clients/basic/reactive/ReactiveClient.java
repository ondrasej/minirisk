/*
A simple reactive minirisk player.
Copyright (C) 2006  Jiri Isa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package cz.matfyz.minirisk.clients.basic.reactive;

import cz.matfyz.minirisk.client.Client;

/** This is a simple reactive minirisk player.
  * It contains a preprogrammed reaction to every card on the table.
  * The reaction may be shifted by a fixed amount.
  * @author Jiri Isa
  */
public class ReactiveClient extends Client {
	/** The reactive strategy. */
	private int[] strategy = { 	10,	// reacts to -5
					8,	//           -4
					6,	//           -3
					4,	//           -2
					2,	//           -1
					1,	//            1
					3,	//            2
					5,	// and so on
					7,
					9,
					11,
					12,
					13,
					14,
					15 };

	/** The basic strategy can be shifted by a fixed shift value. */
	protected static int strategyShift = 0;

	/** Creates a new instance of the player.
	  * @throws Exception In case of the connection problem.
	  */
	public ReactiveClient() throws Exception {
		this("ReactiveClient" + strategyShift);
	}

	/** Creates a new instance of the player.
	  * @param name Name of this client.
	  * @throws Exception In case of the connection problem.
	  */
	public ReactiveClient(String name) throws Exception {
		super(name);
	}

	/** Creates a new instance of the player.
	  * @param hostname Game server host name.
	  * @param port Game server port.
	  * @throws Exception In case of the connection problem.
	  */
	public ReactiveClient(String hostname, int port) throws Exception {
		this(hostname, port, "ReactiveClient" + strategyShift);
	}

	/** Creates a new instance of the player.
	  * @param hostname Game server host name.
	  * @param port Game server port.
	  * @param name Name of this client.
	  * @throws Exception In case of the connection problem.
	  */
	public ReactiveClient(String hostname, int port, String name) throws Exception {
		super(hostname, port, name);
	}

	/** The function invoked to react to the cards on the table.
	  * If there are more cards on the table, only the last one (the new one) is taken into account,
	  * because the reactions to the other cards were already played.
	  */
	public int play(int[] cards) {
		int card = cards[cards.length - 1];
		int cardIndex = (card < 0 ? card - MIN_CARD : card - MIN_CARD - 1);
		return (strategy[cardIndex] - 1 + strategyShift) % CARDS_COUNT +1;
	}

	/** Runs the player.
	  * @param args The command line parameters.
	  * <ul>
	  *   <li>The first one may be the strategy shift. By default 0.</li>
	  *   <li>The second one could be the game server hostname. By default "localhost".</li>
	  *   <li>The third one could be the game server port. By default 7654.</li>
	  * </ul>
	  * If any parameter is to be used, all preceding ones have to be specified.
	  */
	public static void main(String[] args) throws Exception {
		if(args.length > 0) {
			strategyShift = Integer.parseInt(args[0]);
		}

		String hostname = (args.length > 1 ? args[1] : DEFAULT_HOSTNAME);
		int port = (args.length > 2 ? Integer.parseInt(args[2]) : DEFAULT_PORT);

		Client client = new ReactiveClient(hostname, port);
		client.run();
	}
}
