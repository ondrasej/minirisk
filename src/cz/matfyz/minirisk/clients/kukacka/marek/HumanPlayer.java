package cz.matfyz.minirisk.clients.kukacka.marek;

import cz.matfyz.minirisk.client.Client;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Marek Kukacka, November 2009
 */
public class HumanPlayer  extends Client {

	protected ArrayList<Integer> karty = new ArrayList<Integer>(15);
	protected int totalGain = 0;
	protected int gameGain = 0;

	public HumanPlayer() throws UnknownHostException, IOException {
		super("HumanPlayer");
	}

	public HumanPlayer(String hostname, int port) throws UnknownHostException, IOException {
		super(hostname, port, "HumanPlayer");
	}

	protected void start() {
		karty = new ArrayList<Integer>(15);

		for (int i = 0; i < 15; ++i) {
			karty.add(new Integer(i + 1));
		}

		gameGain = 0;
		System.out.println("New game started.");
	}

	@Override
	protected int play(int[] cards) {
		System.out.print("Cards on the table: ");
		for (int i = 0; i < cards.length; ++i) {
			System.out.print(""+cards[i]);
			if(i < (cards.length - 1)) System.out.print(", ");
		}
		System.out.println("");

		System.out.print("Your cards: ");
		for(int i = 0; i < karty.size(); ++i){
			System.out.print(""+karty.get(i));
			if(i < (karty.size() - 1)) System.out.print(", ");
		}
		System.out.println("");

		System.out.println("Which card do you want to play?");
		Scanner keyboard = new Scanner(System.in);
		int n1;
		boolean found = false;
		do{
			n1 = keyboard.nextInt();

			for(int i = 0; i < karty.size(); ++i){
				if(karty.get(i).intValue() == n1){
					karty.remove(i);
					found = true;
					break;
				}
			}
			
			if(found) break;
			else {
				System.out.println("You do not have such a card!");
			}

		} while(true);

		return n1;
	}

	protected void bids(int[] values) {
		System.out.print("Bids: ");
		for (int i = 0; i < values.length; i++) {
			System.out.print(values[i]);

			if (i != values.length - 1) {
				System.out.print(',');
			}
		}
		System.out.println();
	}

	/** This function is invoked to inform the player about the gain obtained in the last round.
	 * @param value The obtained points.
	 */
	protected void gains(int [] value) {
		totalGain += value[getID()];
		gameGain += value[getID()];
		System.out.print("Gains: ");
		for (int i = 0; i < value.length; ++i) {
			System.out.print(value[i]);
			if(i < value.length-1) System.out.print(", ");
		}
		System.out.println("");
		System.out.println("You have got " + value[getID()] + " points now, " + gameGain + " points in this game, " + totalGain + " in total.");
		//profit += value;
	}

	public static void main(String args[]) throws UnknownHostException, IOException {
		String hostname = (args.length > 0 ? args[0] : DEFAULT_HOSTNAME);
		int port = (args.length > 1 ? Integer.parseInt(args[1]) : DEFAULT_PORT);

		Client client = new HumanPlayer(hostname, port);

		client.run();
	}
}
