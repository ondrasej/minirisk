/*
The basic minirisk player implementation.
Copyright (C) 2006  Jiri Isa

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package cz.matfyz.minirisk.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/** An abstract class implementing the basic minirisk player interface
  * and the main program loop.
  * @author Jiri Isa
  */
public abstract class Client {
	/** Number of cards in the game. In the same time the number of game steps. */
	protected final static int CARDS_COUNT = 15;

	/** The value of the lowest card. */
	protected final static int MIN_CARD = -5;

	/** The value of the highest card. */
	protected final static int MAX_CARD = 10;

	/** The default hostname. */
	protected final static String DEFAULT_HOSTNAME = "localhost";
	/** The dafault port number.*/
	protected final static int DEFAULT_PORT = 7654;

	/** A message received when the tournament is over. */
	private final static String QUIT_MESSAGE = "quit";

	/** A message received when the game round starts. */
	private final static String START_MESSAGE = "start";

	/** A message received when the game round is over. */
	private final static String END_MESSAGE = "end";

	/** The socket to the game server. */
	private Socket socket;

	/** The reading end of the game server socket. */
	private BufferedReader in;

	/** The writing end of the game socket. */
	private PrintWriter out;

	/** The count of players in the game. */
	private int playersCount;

	/** The identification number of the player. */
	private int id;

	/** The thinking timeout.
	  * Zero means no timeout.
	  */
	private int timeout;

	/** Creates a new instance of the player with the default server hostname and port.
	  * The default hostname is "localhost" and the default port is 7654.
	  * @param nickname The identification string of this player.
	  * @throws UnknownHostException When the requested server hostname is unknown.
	  * @throws IOException Connection initialization failed.
	  */
	public Client(String nickname) throws UnknownHostException, IOException {
		this(DEFAULT_HOSTNAME, DEFAULT_PORT, nickname);
	}

	/** Creates a new instance of the player with the given server hostname and port.
	  * @param hostname The game server hostname.
	  * @param port The game server port.
	  * @param nickname The identification string of this player.
	  * @throws UnknownHostException When the requested server hostname is unknown.
	  * @throws IOException Connection initialization failed.
	  */
	public Client(String hostname, int port, String nickname) throws UnknownHostException, IOException {
		socket = new Socket(hostname, port);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);

		out.println(nickname);
		playersCount = Integer.parseInt(receive());
		id = Integer.parseInt(receive());
		timeout = Integer.parseInt(receive());
	}

	/** Starts the tournament cycle of the player.
	  */
	public void run() {
		try {
			String message;
			while((message = receive()) != null){
				if(message.equals(QUIT_MESSAGE)) {
					// the tournament is over
					in.close();
					out.close();
					socket.close();
					quit();
					break;
				}

				if(message.equals(START_MESSAGE)) {
					// a game round starts
					start();
					continue;
				}

				if(message.equals(END_MESSAGE)) {
					// a game round ends
					end();
					continue;
				}

				reactToCards(receiveCards(message));

				receiveBids();

				receiveGains();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** Receives a single message (one line) from the game server.
	  * @return Returns the message from the game server.
	  * throws IOException Input error.
	  */
	protected String receive() throws IOException {
		String message = in.readLine();
		return message;
	}

	/** Sends a message to the game server.
	  * @param message The message to be sent.
	  */
	protected void send(String message) {
		out.println(message);
	}

	/** Gets the number of players in the game.
	  * @return Returns the number of players in the game.
	  */
	public int getPlayersCount() {
		return playersCount;
	}

	/** Gets the identification number of this player.
	  * @return This player's ID.
	  */
	public int getID() {
		return id;
	}

	/** Gets the thinking timeout (in milliseconds).
	  */
	public int getTimeout() {
		return timeout;
	}

	/** A helper function which converts a comma-separated integers to an array.
	 * @param s Comma-separated values.
	 * @return A corresponding array.
	 */
	private int[] strAsInts(String s) {
		String[] sValues = s.split(",");
		int[] values = new int[sValues.length];
		for(int i = 0;  i < sValues.length; i++) {
			values[i] = Integer.parseInt(sValues[i]);
		}

		return values;
	}

	/** Parses the information about the cards on the table.
	  * @param message The comma separated list of the card values.
	  * @return An array of the card values.
	  */
	private int[] receiveCards(String message) {
		return strAsInts(message);
	}

	/** Invokes the thinking reaction and sends the result to the game server.
	  * @param cards The card values.
	  * @throws IOException An output problem.
	  * @see play
	  */
	private void reactToCards(int[] cards) throws IOException {
		int reaction = play(cards);
		send(Integer.toString(reaction));
	}

	/** Receives the bids of all players reacting to the last card on the table.
	  * After the message is parsed the reaction function is invoked.
	  * @throws IOException Input error.
	  * @see bids
	  */
	private void receiveBids() throws IOException {
		String msg = receive();
		int[] bs = strAsInts(msg);
		bids(bs);
	}

	/** Receives the gains of all players.
	  * After the message is parsed the reaction function is invoked.
	  * @throws IOException Input error.
	  * @see gains
	  */
	private void receiveGains() throws IOException {
		String msg = receive();
		int[] gs = strAsInts(msg);
		gains(gs);
	}

	/** This function is invoked when the tournament is over. */
	protected void quit(){};

	/** This function is invoked when the game round start. */
	protected void start(){};

	/** This function is invoked when the game round ends. */
	protected void end(){};

	/** This function is requested to decide what to play.
	  * @param cards The cards on the table that can be won in the current step.
	  * @return The played card.
	  */
	protected abstract int play(int[] cards);

	/** This function informs the player about the situation on the table.
	  * After the central card is placed on the table, this function informs about all the reactions.
	  * This player's reaction is on the index equal to this player's ID.
	  * @param values The values offered by all players.
	  * @see getID
	  */
	protected void bids(int[] values){};

	/** This function is invoked to inform the player about how much everyone has won in the game step.
	  * @param values The amounts of points won in the last round.
	  */
	protected void gains(int[] values){};
}
