#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;

my %clients;
my %scores;
my %total_scores;

my $verbose = 0;

GetOptions(
    "verbose" => \$verbose
);

sub process_file {
    my ($file) = @_;

    my $lines = `tail -n 3 $file`;
    print STDERR "$lines\n" if $verbose;
    my @lines = split /\n/, $lines;

    # if there is no 'quit' command, do not try to process the scores
    return unless $lines[2] eq 'quit';

    $lines[0] =~ /total_gain 0 "([^"]*)" ([-0-9]*)/;
    my ($name1, $score1) = ($1, $2);
    $lines[1] =~ /total_gain 1 "([^"]*)" ([-0-9]*)/;
    my ($name2, $score2) = ($1, $2);

    $clients{$name1} = 1;
    $clients{$name2} = 1;

    print STDERR "$name1--$name2 : $score1 - $score2\n" if $verbose;
    print STDERR "$name2--$name1 : $score2 - $score1\n" if $verbose;

    $scores{$name1} = {} unless $scores{$name1};
    print STDERR "Score already exists: $name1--$name2\n" if exists $scores{$name1}{$name2};
    $scores{$name1}{$name2} = $score1;
    $scores{$name2} = {} unless $scores{$name2};
    $scores{$name2}{$name1} = $score2;

    $total_scores{$name1} = 0 unless $total_scores{$name1};
    $total_scores{$name1} += $score1;
    $total_scores{$name2} = 0 unless $total_scores{$name2};
    $total_scores{$name2} += $score2;
}

my @files = glob("*.txt");
for my $fn (@files) {
    process_file $fn;
}

my @clients = sort keys %clients;
my @clients_by_score = sort { $total_scores{$b} - $total_scores{$a} } @clients;
print "\tTotal\t".join("\t", @clients)."\n";
for my $key1 (@clients_by_score) {
    print $key1;
    print "\t".$total_scores{$key1};
    for my $key2 (@clients) {
	my $score = $scores{$key1}{$key2} || "";
	print "\t$score";
    }
    print "\n";
}
