@ECHO OFF

IF NOT EXIST bin MKDIR bin

REM You might need to change this to your location of javac.exe for the batch to work properly.
SET javacpath=""

SET javacparams="bin;lib/jglpk.jar;lib/jnash.jar;lib/jna.jar"

%javacpath%javac -d bin -cp %javacparams% src\cz\matfyz\minirisk\client\Client.java ^
 src\cz\matfyz\minirisk\clients\basic\random\RandomClient.java ^
 src\cz\matfyz\minirisk\clients\basic\reactive\ReactiveClient.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\AFocusedPessimistClient.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\Factor.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\GameState.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\GameTree.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\Marvin.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\McMarvin.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\MixedStrategy.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\Situation.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\Strategy.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\afp\VonMarvin.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\naiveMCL\NaiveMCLClient.java ^
 src\cz\matfyz\minirisk\clients\isa\jiri\simpleRL\SimpleRLClient.java ^
 src\cz\matfyz\minirisk\clients\kukacka\marek\HumanPlayer.java ^
 src\cz\matfyz\minirisk\clients\sykora\ondrej\ClientUtils.java ^
 src\cz\matfyz\minirisk\clients\sykora\ondrej\MemoryClient.java ^
 src\cz\matfyz\minirisk\clients\sykora\ondrej\MemoryEstimateClient.java ^
 src\cz\matfyz\minirisk\clients\sykora\ondrej\MemoryMCClient.java ^
 src\cz\matfyz\minirisk\clients\sykora\ondrej\MixedStrategyClient.java ^
 src\cz\matfyz\minirisk\server\ReplayServer.java ^
 src\cz\matfyz\minirisk\server\Server.java
