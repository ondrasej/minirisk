-- A really trivial Haskell Minirisk client
import Minirisk

-- A reactive client that is defined by its strategy, i.e.
-- by a mapping from value cards to currency cards.
data ReactiveClient = ReactiveClient [CurrencyCard]
instance Player ReactiveClient where
    playerName _ = "HsReactiveClient"

    -- Responds to the last card sent by the server.
    play (ReactiveClient strategy) cards = return card
      where
        valCard = valueCardValue$ last cards
        cardIndex = if valCard > 0
                        then valCard + 4
                        else valCard + 5
        card = strategy !! cardIndex

main = do
    -- TODO: read the options from command-line
    let options = ServerOptions "localhost" 7654
    -- Create a new reactive client with a trivial strategy
    let client = ReactiveClient (map (currencyCard) ([1..15]))
    -- Run the client
    runClient options client
