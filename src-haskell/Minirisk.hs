-- | The interface for Minirisk client programs. This package provides
-- either the interface, and a function that connects to the server and
-- runs the game using an implementation of that interface.
--
-- To create a new Minirisk client, create a new instance of the 'Player'
-- class, and use it with the 'runClient' function.
module Minirisk (ServerOptions(..)
                ,CurrencyCard, currencyCard, currencyCardValue
                ,ValueCard, valueCard, valueCardValue
                ,Player(..)
                ,runClient
                ) where

import Control.Monad
import Network.BSD
import Network.Socket
import System.IO

-- | Specifies the hostname and the port of the Minirisk server.
data ServerOptions = ServerOptions {
                            serverHostName :: String,
                            serverPort :: PortNumber
                        }

-- | A currency card.
newtype CurrencyCard = CCard Int
instance Eq CurrencyCard where
    CCard a == CCard b      = a == b
instance Ord CurrencyCard where
    compare (CCard a) (CCard b) = compare a b
instance Show CurrencyCard where
    show (CCard a)          = show a

-- | Creates a currency card of the given value. The value must be a number between
-- 1 and 15.
currencyCard    :: Int -> CurrencyCard
currencyCard value
    | value >= 1 && value <= 15     = CCard value
    | otherwise                     = error$ "The payment card value must be a "
                                            ++ "number between 1 and 15"

-- | Returns the value of the given currency card.
currencyCardValue   :: CurrencyCard -> Int
currencyCardValue (CCard value) = value


-- | A value card.
newtype ValueCard = VCard Int
instance Eq ValueCard where
    VCard a == VCard b      = a == b
instance Ord ValueCard where
    compare (VCard a) (VCard b) = compare a b
instance Show ValueCard where
    show (VCard a)          = show a

-- | Creates a value card of the given value. The value must be a number between
-- -5 and -1, or a number between 1 and 10.
valueCard :: Int -> ValueCard
valueCard value
    | value >= -5 && value <= -1    = VCard value
    | value >= 1 && value <= 10     = VCard value
    | otherwise                     = error$ "The value must be a number between "
                                            ++ "-5 and -1, or a number between 1 "
                                            ++ "and 10"

-- | Returns the value of the given value card.
valueCardValue :: ValueCard -> Int
valueCardValue (VCard value) = value


-- | The common interface for Minirisk clients. See the descriptions
-- of the methods for more details.
-- 
-- The minimal complete definition contains 'playerName' and 'play'.
class Player a where
    -- | Performs the necessary initialization of the player before
    -- the game starts. Gives the player its own ID, the number of
    -- players and the timeout for each bid as parameters.
    initialize  :: a        -- ^ The client being initialized 
                -> Int      -- ^ The ID of the client (as reported
                            --   by the server)
                -> Int      -- ^ The number of players in the game
                -> Int      -- ^ The time limit for each round (in
                            --   miliseconds)
                -> IO ()
    initialize _ _ _ _ = return ()

    -- | Returns the name of the player (name of the player class)
    -- for the given instance of player (so that different instances
    -- can have different names, e.g. based on the initialization
    -- parameters.
    playerName  :: a        -- ^ The client, for which the name is
                            --   returned.
                -> String   -- ^ The name of the client
    
    -- | Determines, which card should be played in the current bid.
    -- Gets information about the current cards on the table as a
    -- parameters, and returns the currency card for the current
    -- bid.
    play    :: a                -- ^ The client, for which the bid
                                --   is performed
            -> [ValueCard]      -- ^ The list of value cards that are
                                --   on the table in the current round
            -> IO CurrencyCard  -- ^ The currency card selected for
                                --   bidding in the current round

    -- | Reports the bids and gains of all players to the player. The
    -- player can use it to update its own knowledge about the players,
    -- to learn from the gains, ...
    -- The default implementation of this method does nothing.
    processBids :: a                -- ^ The client that processes the
                                    --   bids
                -> [CurrencyCard]   -- ^ The bids submitted for the
                                    --   current round by all players.
                                    --   The bids in the list are in
                                    --   the same order as if they were
                                    --   indexed by the player IDs
                -> [Int]            -- ^ The outcomes of all players,
                                    --   in the same order as the cards
                                    --   in the previous parameter.
                -> IO ()
    processBids _ _ _ = return ()

    -- | Tells the player that a new round of the game has started.
    -- Gives the number of the round to the player as a parameter.
    -- The default implementation of this method does nothing.
    startGame   :: a        -- ^ The client that is notified that the
                            --   game has started
                -> IO ()
    startGame _ = return ()

    -- | Tells the player that the current round of the game has ended.
    -- The default implementation of this method does nothing.
    endGame :: a        -- ^ The client that is notified that the game
                        --   has ended
            -> IO ()
    endGame _ = return ()

    -- Performs the necessary cleanup after the whole match has finished
    -- (i.e. after all rounds of the game).
    quit    :: a        -- ^ The client that performs the cleanup
            -> IO ()
    quit _ = return ()


-- | Connects to the Minirisk server according to the options, and plays
--   the game using the given client object. This is the main method, that
--   should be called from the main function of the Minirisk client programs.
--
-- > main = do
-- >    let client = createMyMiniriskClient
-- >    let server = determineServerParameters
-- >    runClient server client
runClient   :: Player player
            => ServerOptions    -- ^ Information about the Minirisk server
                                --   for the game (server hostname and port)
            -> player           -- ^ The Minirisk client
            -> IO ()
runClient options client = withSocketsDo$ do
    -- Connect to the server
    hPutStr stderr "Connecting to server... "
    (serverAddress, protocol) <- resolveServerAddress
    serverSocket <- socket AF_INET Stream protocol
    connect serverSocket serverAddress
    server <- socketToHandle serverSocket ReadWriteMode
    hSetBuffering server NoBuffering
    hPutStrLn stderr "OK"

    -- Send the player name to the server, read the parameters of the
    -- game (number of players, player ID, timeout)
    hPutStrLn server (playerName client)
    playerCount <- (receive server) :: IO Int
    playerId <- (receive server) :: IO Int
    timeout <- (receive server) :: IO Int

    -- Initialize the client
    initialize client playerId playerCount timeout

    -- Run the message loop
    process_messages server

    -- De-initialize the client
    quit client

    -- Cleanup. Diconnect from server, and exit
    hClose server
    return ()
  where
    -- Implementation of the message loop. Repeat, until the server
    -- sends the 'quit' message.
    process_messages server = do
        message <- hGetLine server
        should_continue <- process_message server message
        when should_continue (process_messages server)

    -- Processes a single message from the server. Returns a boolean
    -- value: True, if the message loop should continue, False if this
    -- was the last mesasge.
    process_message :: Handle -> String -> IO Bool
    process_message _ "quit" = do
        -- Quit message - leave the message loop
        return False
    process_message _ "start" = do
        -- Start game message - notify the client that a new game has started
        hPutStrLn stderr "Starting game"
        startGame client
        return True
    process_message _ "end" = do
        -- End game message - notify the client that the current game has ended
        hPutStrLn stderr "Ending game"
        endGame client
        return True
    process_message server msg = do
        -- Game message - the server sent the list of value cards on the table,
        -- and expects the client to send the bid. This function handles the
        -- exchange for a single round.
        hPutStrLn stderr$ "Msg: " ++ msg
        let vcInt = read$ "[" ++ msg ++ "]" :: [Int]
        let valueCards = map (valueCard) vcInt
        bid_card <- play client valueCards
        hPutStrLn server $ show (currencyCardValue bid_card)
        all_bids <- receiveList server
        incomes <- receiveList server
        let bidCards = map (currencyCard) all_bids
        processBids client bidCards incomes
        return True

    -- Resolves the hostname and protocol number for the connection to the
    -- Minirisk server
    resolveServerAddress :: IO (SockAddr, ProtocolNumber)
    resolveServerAddress = do
        protocol <- getProtocolByName "tcp"
        serverHost <- getHostByName$ serverHostName options
        let address = SockAddrInet (serverPort options) (hostAddress serverHost)
        let protocolNumber = protoNumber protocol
        return$ (address, protocolNumber)

    -- Reads a single value from the server (reads a line of text from the
    -- server and then parses the value of the requested type
    receive :: Read a => Handle -> IO a
    receive server = do
        line <- hGetLine server
        return$ read line

    -- Reads a list of comma-separated values from the server. Reads a line
    -- of text from the server and converts it into a list
    receiveList :: Read a => Handle -> IO [a]
    receiveList server = do
        line <- hGetLine server
        let list = read$ "[" ++ line ++ "]"
        return list
