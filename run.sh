#!/bin/sh

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
	echo "Usage: run.sh class1 class2 log [port]"
	echo "Where class1 is the class of the first player"
	echo "      class2 is the class of the second player"
	echo "      log is the name of the file, to which the server log is saved"
	echo "      port is the number of the port used for the game"
	exit 1
fi

CLASSPATH_PARAM="-cp bin:lib/jglpk.jar:lib/jnash.jar:lib/jna.jar"

SERVER_PARAMS=""
CLIENT_PARAMS=""
if [ -n "$4" ]; then
	SERVER_PARAMS="-p $4"
	CLIENT_PARAMS="localhost $4"
fi

java ${CLASSPATH_PARAM} cz.matfyz.minirisk.server.Server $SERVER_PARAMS > "$3" &
SERVER_PID=$!

sleep 5

java -Xmx1G ${CLASSPATH_PARAM} $1 $CLIENT_PARAMS > /dev/null 2> "$3"--1.log &

sleep 5

java -Xmx1G ${CLASSPATH_PARAM} $2 $CLIENT_PARAMS > /dev/null 2> "$3"--2.log &

wait $SERVER_PID

END=`tail -n 1 "$3"`
if [ "$END" == "quit" ]; then
	rm "$3"--1.log
	rm "$3"--2.log
fi
